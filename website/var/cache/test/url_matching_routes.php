<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'carte', '_controller' => 'App\\Controller\\MateteController::carte'], null, null, null, false, false, null]],
        '/lesAnnonces' => [[['_route' => 'lesAnnonces', '_controller' => 'App\\Controller\\MateteController::index'], null, null, null, false, false, null]],
        '/nouveau' => [[['_route' => 'annonce_new', '_controller' => 'App\\Controller\\MateteController::add'], null, null, null, false, false, null]],
        '/panier' => [[['_route' => 'panier', '_controller' => 'App\\Controller\\MateteController::showPanier'], null, null, null, false, false, null]],
        '/pdf' => [[['_route' => 'pdf', '_controller' => 'App\\Controller\\MateteController::pdf'], null, null, null, false, false, null]],
        '/inscription' => [[['_route' => 'new_inscription', '_controller' => 'App\\Controller\\SecurityController::registration'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/users' => [[['_route' => 'list_users', '_controller' => 'App\\Controller\\SecurityController::users'], null, null, null, false, false, null]],
        '/gestionCategorie' => [[['_route' => 'gestion_categorie', '_controller' => 'App\\Controller\\SecurityController::gererCategorie'], null, null, null, false, false, null]],
        '/showProfil' => [[['_route' => 'profil_show', '_controller' => 'App\\Controller\\SecurityController::showProfil'], null, null, null, false, false, null]],
        '/deleteProfil' => [[['_route' => 'profil_delete', '_controller' => 'App\\Controller\\SecurityController::deleteProfil'], null, null, null, false, false, null]],
        '/statistiques' => [[['_route' => 'stats', '_controller' => 'App\\Controller\\SecurityController::stats'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/a(?'
                    .'|nnonce/([^/]++)(*:27)'
                    .'|jouter_panier/([^/]++)/([^/]++)(*:65)'
                .')'
                .'|/edit/([^/]++)(*:87)'
                .'|/delete(?'
                    .'|/([^/]++)(*:113)'
                    .'|Panier/([^/]++)/([^/]++)(*:145)'
                    .'|Categorie/([^/]++)(*:171)'
                    .'|User/([^/]++)(*:192)'
                    .'|Emplacement/([^/]++)(*:220)'
                .')'
                .'|/suspendUser/([^/]++)(*:250)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        27 => [[['_route' => 'annonce_show', '_controller' => 'App\\Controller\\MateteController::show'], ['id'], null, null, false, true, null]],
        65 => [[['_route' => 'ajouter_panier', '_controller' => 'App\\Controller\\MateteController::addPanier'], ['id', 'from'], null, null, false, true, null]],
        87 => [[['_route' => 'annonce_edit', '_controller' => 'App\\Controller\\MateteController::edit'], ['id'], null, null, false, true, null]],
        113 => [[['_route' => 'annonce_delete', '_controller' => 'App\\Controller\\MateteController::deleteAction'], ['id'], null, null, false, true, null]],
        145 => [[['_route' => 'panier_delete', '_controller' => 'App\\Controller\\MateteController::deletePanier'], ['id', 'from'], null, null, false, true, null]],
        171 => [[['_route' => 'categorie_delete', '_controller' => 'App\\Controller\\SecurityController::delCategorie'], ['id'], null, null, false, true, null]],
        192 => [[['_route' => 'user_delete', '_controller' => 'App\\Controller\\SecurityController::delUser'], ['id'], null, null, false, true, null]],
        220 => [[['_route' => 'emplacement_delete', '_controller' => 'App\\Controller\\SecurityController::delEmplacement'], ['id'], null, null, false, true, null]],
        250 => [
            [['_route' => 'user_suspend', '_controller' => 'App\\Controller\\SecurityController::suspendUser'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];

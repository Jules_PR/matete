<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerOyPjP7T\App_KernelTestDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerOyPjP7T/App_KernelTestDebugContainer.php') {
    touch(__DIR__.'/ContainerOyPjP7T.legacy');

    return;
}

if (!\class_exists(App_KernelTestDebugContainer::class, false)) {
    \class_alias(\ContainerOyPjP7T\App_KernelTestDebugContainer::class, App_KernelTestDebugContainer::class, false);
}

return new \ContainerOyPjP7T\App_KernelTestDebugContainer([
    'container.build_hash' => 'OyPjP7T',
    'container.build_id' => '5a45dd91',
    'container.build_time' => 1647866084,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerOyPjP7T');

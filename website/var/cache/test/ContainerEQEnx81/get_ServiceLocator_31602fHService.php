<?php

namespace ContainerEQEnx81;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_31602fHService extends App_KernelTestDebugContainer
{
    /**
     * Gets the private '.service_locator.31602fH' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.31602fH'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'categorieRepo' => ['privates', 'App\\Repository\\CategorieRepository', 'getCategorieRepositoryService', true],
            'emplacementRepo' => ['privates', 'App\\Repository\\EmplacementRepository', 'getEmplacementRepositoryService', true],
            'entityManager' => ['services', 'doctrine.orm.default_entity_manager', 'getDoctrine_Orm_DefaultEntityManagerService', false],
        ], [
            'categorieRepo' => 'App\\Repository\\CategorieRepository',
            'emplacementRepo' => 'App\\Repository\\EmplacementRepository',
            'entityManager' => '?',
        ]);
    }
}

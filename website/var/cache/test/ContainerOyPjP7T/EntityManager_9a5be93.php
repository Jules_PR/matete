<?php

namespace ContainerOyPjP7T;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderdd187 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer97011 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties2aa88 = [
        
    ];

    public function getConnection()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getConnection', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getMetadataFactory', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getExpressionBuilder', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'beginTransaction', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getCache', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getCache();
    }

    public function transactional($func)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'transactional', array('func' => $func), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'wrapInTransaction', array('func' => $func), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'commit', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->commit();
    }

    public function rollback()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'rollback', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getClassMetadata', array('className' => $className), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'createQuery', array('dql' => $dql), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'createNamedQuery', array('name' => $name), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'createQueryBuilder', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'flush', array('entity' => $entity), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'clear', array('entityName' => $entityName), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->clear($entityName);
    }

    public function close()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'close', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->close();
    }

    public function persist($entity)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'persist', array('entity' => $entity), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'remove', array('entity' => $entity), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'refresh', array('entity' => $entity), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'detach', array('entity' => $entity), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'merge', array('entity' => $entity), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getRepository', array('entityName' => $entityName), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'contains', array('entity' => $entity), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getEventManager', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getConfiguration', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'isOpen', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getUnitOfWork', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getProxyFactory', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'initializeObject', array('obj' => $obj), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'getFilters', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'isFiltersStateClean', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'hasFilters', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return $this->valueHolderdd187->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer97011 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderdd187) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderdd187 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderdd187->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, '__get', ['name' => $name], $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        if (isset(self::$publicProperties2aa88[$name])) {
            return $this->valueHolderdd187->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd187;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderdd187;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd187;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderdd187;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, '__isset', array('name' => $name), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd187;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderdd187;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, '__unset', array('name' => $name), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderdd187;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderdd187;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, '__clone', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        $this->valueHolderdd187 = clone $this->valueHolderdd187;
    }

    public function __sleep()
    {
        $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, '__sleep', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;

        return array('valueHolderdd187');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer97011 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer97011;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer97011 && ($this->initializer97011->__invoke($valueHolderdd187, $this, 'initializeProxy', array(), $this->initializer97011) || 1) && $this->valueHolderdd187 = $valueHolderdd187;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderdd187;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderdd187;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/carte.html.twig */
class __TwigTemplate_a6a6008edc6fa22b0a57a4b6de18292d1c1c89567039fef24fc5d4ff6de60c69 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/carte.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/carte.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/carte.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<!DOCTYPE html>
<html>
  <head>
  <script>
    markers = [];
    var userMarker = null;

    function initMap() {

    const perpignan = { lat: 42.689658, lng: 3.033439 };
    const map = new google.maps.Map(document.getElementById(\"map\"), {
        zoom: 10,
        center: perpignan,
        disableDefaultUI: true,
        zoomControl: true
    });

    var infowindow = new google.maps.InfoWindow();
    var markerAnnonce, i;

    var n = 0;
    var xhr = []

    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 26, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["annonce"]) {
            // line 27
            echo "      adresse = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "emplacement", [], "any", false, false, false, 27), "adresse", [], "any", false, false, false, 27), "html", null, true);
            echo "';
      codePostal = '";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "emplacement", [], "any", false, false, false, 28), "codePostal", [], "any", false, false, false, 28), "html", null, true);
            echo "';
      adresse = adresse.replace(/ /g, \"+\");

      //construit l'url qui servira a récupérer les latitudes longitudes des adresses des annonces
      var requestUrl = \"https://api-adresse.data.gouv.fr/search/?q=\" + adresse + \"&postcode=\" + codePostal
      
      //fait la requête à l'API gouv, il faut une instantation de XMLHttpRequest par requête d'ou l'index [n]
      xhr[n] = new XMLHttpRequest();
      xhr[n].open('GET', requestUrl);
      xhr[n].responseType = 'json';
      xhr[n].send();

      xhr[n].onload = function() {
        var results = this.response;
        var lat = results.features[0].geometry.coordinates[1];
        var lng = results.features[0].geometry.coordinates[0];
        var city = results.features[0].properties.city;
        var id = '";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 45), "html", null, true);
            echo "'
        var nom = '";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "nom", [], "any", false, false, false, 46), "html", null, true);
            echo "'
        var img = '";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "image", [], "any", false, false, false, 47), "html", null, true);
            echo "'

        //stock la data nécessaire pour le marker de l'annonce dans cet array
        var annonceMarkerDATA = [id, nom, lat, lng, img]

        //définit la position du marker avec lat et lng récupérés précédemment        
        var pos = {
        lat: annonceMarkerDATA[2],
        lng: annonceMarkerDATA[3],
        }
        //pose le point
        markerAnnonce = new google.maps.Marker({
          title: '";
            // line 59
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "nom", [], "any", false, false, false, 59), "html", null, true);
            echo "',
          position: pos,
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
          categorie: '";
            // line 63
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "lacategorie", [], "any", false, false, false, 63), "nom", [], "any", false, false, false, 63), "html", null, true);
            echo "',
          city: city,
        });

        google.maps.event.addListener(markerAnnonce, 'click', (function(markerAnnonce, i) {
          return function() {
            infowindow.setContent(\"<a class='h3 text-dark simple-link center' href='/annonce/\" + annonceMarkerDATA[0] + \"'>\" + annonceMarkerDATA[1] + \"<br><img src='\" + annonceMarkerDATA[4] + \"' alt='Image de l'annonce' style='border-radius:5px' width='300px''></a>\");
            infowindow.open(map, markerAnnonce);
          }
        })(markerAnnonce, i));
          n = n + 1;
          markers.push(markerAnnonce);
        }
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annonce'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
      (position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        
        var markerUser = map.setCenter(pos);
        var leMarkerUser = new google.maps.Marker({
          position: pos,
          map : map,
          title: 'Vous êtes ici'
        });
        userMarker = leMarkerUser;
      },
      () => {
        handleLocationError(true, infowindow, map.getCenter());
      }
    );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infowindow, map.getCenter());
    }
}
  </script>
  </head>
  <body>
    <div id=\"map\"></div>
    <div>
    <div class=\"card border-primary mb-3\" style=\"max-width: 2rem;z-index:2;position:absolute;top:9.2%;left:0.40%;max-height:2rem;cursor:pointer;\" id='search' onclick='show_search_panel()'>
      <div class=\"card-body\">
        <p class='card-text' style='position:absolute;top:3.5%;right:20%'><strong><i class='uil uil-search'></i></strong></p>
      </div>
    </div>

    <div class=\"card border-primary mb-3\" style=\"max-width:22rem;z-index:1;position:absolute;top:9.2%;left:0.40%;display:none;\" id='card'>
    <div class=\"card-body\">
    <div style='position:absolute;right:0%;top:0%;cursor:pointer'><i class='uil uil-times' onclick='dismiss_search_panel();'></i></div>
      <input type='text' class='form-control' placeholder='Rechercher...' style='width:100%' oninput='recherche(this)'>
      <select class='form-control mt-2' onchange='filter_categorie(this);'>
      <option>Toutes catégories</option>
      ";
        // line 120
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 120, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
            // line 121
            echo "        <option>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categorie"], "nom", [], "any", false, false, false, 121), "html", null, true);
            echo "</option>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "      </select>
      <select class='form-control mt-2' onchange='distance_filter(this);'>
      <option>Pas de limite</option>
      <option>2km</option>
      <option>5km</option>
      <option>10km</option>
      <option>20km</option>
      <option>30km</option>
      <option>50km</option>
      <option>100km</option>
      </select>
    </div>
    </div>
    <script
      src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyD5hijwTxtQCINhGfTZ4HJhMutqHk86COs&callback=initMap&libraries=geometry&v=3\"
    ></script>
    <script>
    function dismiss_search_panel(){
      document.getElementById('card').style.display='none';
      document.getElementById('search').style.display='block';
    }

    function show_search_panel(){
      document.getElementById('card').style.display='block';
      document.getElementById('search').style.display='none';
    }

    //filtre les markers d'annonce en fonction de leur distance avec le userMarker
    function distance_filter(selectedOption) {
      markersVisibles = [];
      value = selectedOption.value;

      //affiche tous les markers, sinon affiche que ceux en fonction de la limite choisie
      if(value == \"Pas de limite\"){
        show(markers);
      }
      else{
        value = parseInt(value.replace( /^\\D+/g, ''));
        value = value * 1000;
        
        for (var i = 0; i < markers.length; i++) {
          var distance = google.maps.geometry.spherical.computeDistanceBetween(
            markers[i].getPosition(),
            userMarker.getPosition()
          );

          if (distance < value){
            markersVisibles.push(markers[i]);
          }
        }
        show(markersVisibles);
      }
    }

    //filtre en fonction de la categorie choisie
    function filter_categorie(selectedOption){
      markersVisibles = []
      value = selectedOption.value;

      var ToutesLesAnnonces = document.getElementsByClassName('Tous');
      var annoncesVisibles = document.getElementsByClassName(value);

      if(value == 'Toutes catégories'){
        for(var i = 0; i < ToutesLesAnnonces.length; i++){
          ToutesLesAnnonces[i].style.display = 'block';
        }
        show(markers);
      }else{
        for(var i = 0; i < markers.length; i++){
          if(markers[i]['categorie'] == value) {
            markersVisibles.push(markers[i])
          }
        }
        show(markersVisibles)
      }

      for(var i = 0; i < ToutesLesAnnonces.length; i++){
        ToutesLesAnnonces[i].style.display = 'none';
      }

      for(var i = 0; i < annoncesVisibles.length; i++){
        annoncesVisibles[i].style.display = 'block';
      }
    }

    //barre de recherche
    function recherche(input){
      input = input.value
      for (var i = 0; i < markers.length; i++){
        if(markers[i]['title'].toLowerCase().includes(input.toLowerCase()) 
          || markers[i]['categorie'].toLowerCase().includes(input.toLowerCase())
          || markers[i]['city'].toLowerCase().includes(input.toLowerCase())
          || markers[i]['city'].toLowerCase().replace(/[^A-Z0-9]+/ig, \" \").includes(input.toLowerCase())) {
          markers[i].setVisible(true)
        }else{
          markers[i].setVisible(false)
        }
      }
    }

    //cache tous les markers et fait apparaitre seulement ceux passés en paramètre
    function show(markersVisibles){
      for (var i = 0; i < markers.length; i++){
        markers[i].setVisible(false)
      }
      for (var i = 0; i < markersVisibles.length; i++){
        markersVisibles[i].setVisible(true)
      }
    }
    </script>
  </body>
</html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/carte.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 123,  221 => 121,  217 => 120,  172 => 77,  152 => 63,  145 => 59,  130 => 47,  126 => 46,  122 => 45,  102 => 28,  97 => 27,  93 => 26,  68 => 3,  58 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block body %}
<!DOCTYPE html>
<html>
  <head>
  <script>
    markers = [];
    var userMarker = null;

    function initMap() {

    const perpignan = { lat: 42.689658, lng: 3.033439 };
    const map = new google.maps.Map(document.getElementById(\"map\"), {
        zoom: 10,
        center: perpignan,
        disableDefaultUI: true,
        zoomControl: true
    });

    var infowindow = new google.maps.InfoWindow();
    var markerAnnonce, i;

    var n = 0;
    var xhr = []

    {% for annonce in annonces %}
      adresse = '{{ annonce.emplacement.adresse }}';
      codePostal = '{{ annonce.emplacement.codePostal }}';
      adresse = adresse.replace(/ /g, \"+\");

      //construit l'url qui servira a récupérer les latitudes longitudes des adresses des annonces
      var requestUrl = \"https://api-adresse.data.gouv.fr/search/?q=\" + adresse + \"&postcode=\" + codePostal
      
      //fait la requête à l'API gouv, il faut une instantation de XMLHttpRequest par requête d'ou l'index [n]
      xhr[n] = new XMLHttpRequest();
      xhr[n].open('GET', requestUrl);
      xhr[n].responseType = 'json';
      xhr[n].send();

      xhr[n].onload = function() {
        var results = this.response;
        var lat = results.features[0].geometry.coordinates[1];
        var lng = results.features[0].geometry.coordinates[0];
        var city = results.features[0].properties.city;
        var id = '{{ annonce.id }}'
        var nom = '{{ annonce.nom }}'
        var img = '{{ annonce.image }}'

        //stock la data nécessaire pour le marker de l'annonce dans cet array
        var annonceMarkerDATA = [id, nom, lat, lng, img]

        //définit la position du marker avec lat et lng récupérés précédemment        
        var pos = {
        lat: annonceMarkerDATA[2],
        lng: annonceMarkerDATA[3],
        }
        //pose le point
        markerAnnonce = new google.maps.Marker({
          title: '{{ annonce.nom }}',
          position: pos,
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
          categorie: '{{ annonce.lacategorie.nom }}',
          city: city,
        });

        google.maps.event.addListener(markerAnnonce, 'click', (function(markerAnnonce, i) {
          return function() {
            infowindow.setContent(\"<a class='h3 text-dark simple-link center' href='/annonce/\" + annonceMarkerDATA[0] + \"'>\" + annonceMarkerDATA[1] + \"<br><img src='\" + annonceMarkerDATA[4] + \"' alt='Image de l'annonce' style='border-radius:5px' width='300px''></a>\");
            infowindow.open(map, markerAnnonce);
          }
        })(markerAnnonce, i));
          n = n + 1;
          markers.push(markerAnnonce);
        }
    {% endfor %}

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
      (position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        
        var markerUser = map.setCenter(pos);
        var leMarkerUser = new google.maps.Marker({
          position: pos,
          map : map,
          title: 'Vous êtes ici'
        });
        userMarker = leMarkerUser;
      },
      () => {
        handleLocationError(true, infowindow, map.getCenter());
      }
    );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infowindow, map.getCenter());
    }
}
  </script>
  </head>
  <body>
    <div id=\"map\"></div>
    <div>
    <div class=\"card border-primary mb-3\" style=\"max-width: 2rem;z-index:2;position:absolute;top:9.2%;left:0.40%;max-height:2rem;cursor:pointer;\" id='search' onclick='show_search_panel()'>
      <div class=\"card-body\">
        <p class='card-text' style='position:absolute;top:3.5%;right:20%'><strong><i class='uil uil-search'></i></strong></p>
      </div>
    </div>

    <div class=\"card border-primary mb-3\" style=\"max-width:22rem;z-index:1;position:absolute;top:9.2%;left:0.40%;display:none;\" id='card'>
    <div class=\"card-body\">
    <div style='position:absolute;right:0%;top:0%;cursor:pointer'><i class='uil uil-times' onclick='dismiss_search_panel();'></i></div>
      <input type='text' class='form-control' placeholder='Rechercher...' style='width:100%' oninput='recherche(this)'>
      <select class='form-control mt-2' onchange='filter_categorie(this);'>
      <option>Toutes catégories</option>
      {% for categorie in categories %}
        <option>{{categorie.nom}}</option>
      {% endfor %}
      </select>
      <select class='form-control mt-2' onchange='distance_filter(this);'>
      <option>Pas de limite</option>
      <option>2km</option>
      <option>5km</option>
      <option>10km</option>
      <option>20km</option>
      <option>30km</option>
      <option>50km</option>
      <option>100km</option>
      </select>
    </div>
    </div>
    <script
      src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyD5hijwTxtQCINhGfTZ4HJhMutqHk86COs&callback=initMap&libraries=geometry&v=3\"
    ></script>
    <script>
    function dismiss_search_panel(){
      document.getElementById('card').style.display='none';
      document.getElementById('search').style.display='block';
    }

    function show_search_panel(){
      document.getElementById('card').style.display='block';
      document.getElementById('search').style.display='none';
    }

    //filtre les markers d'annonce en fonction de leur distance avec le userMarker
    function distance_filter(selectedOption) {
      markersVisibles = [];
      value = selectedOption.value;

      //affiche tous les markers, sinon affiche que ceux en fonction de la limite choisie
      if(value == \"Pas de limite\"){
        show(markers);
      }
      else{
        value = parseInt(value.replace( /^\\D+/g, ''));
        value = value * 1000;
        
        for (var i = 0; i < markers.length; i++) {
          var distance = google.maps.geometry.spherical.computeDistanceBetween(
            markers[i].getPosition(),
            userMarker.getPosition()
          );

          if (distance < value){
            markersVisibles.push(markers[i]);
          }
        }
        show(markersVisibles);
      }
    }

    //filtre en fonction de la categorie choisie
    function filter_categorie(selectedOption){
      markersVisibles = []
      value = selectedOption.value;

      var ToutesLesAnnonces = document.getElementsByClassName('Tous');
      var annoncesVisibles = document.getElementsByClassName(value);

      if(value == 'Toutes catégories'){
        for(var i = 0; i < ToutesLesAnnonces.length; i++){
          ToutesLesAnnonces[i].style.display = 'block';
        }
        show(markers);
      }else{
        for(var i = 0; i < markers.length; i++){
          if(markers[i]['categorie'] == value) {
            markersVisibles.push(markers[i])
          }
        }
        show(markersVisibles)
      }

      for(var i = 0; i < ToutesLesAnnonces.length; i++){
        ToutesLesAnnonces[i].style.display = 'none';
      }

      for(var i = 0; i < annoncesVisibles.length; i++){
        annoncesVisibles[i].style.display = 'block';
      }
    }

    //barre de recherche
    function recherche(input){
      input = input.value
      for (var i = 0; i < markers.length; i++){
        if(markers[i]['title'].toLowerCase().includes(input.toLowerCase()) 
          || markers[i]['categorie'].toLowerCase().includes(input.toLowerCase())
          || markers[i]['city'].toLowerCase().includes(input.toLowerCase())
          || markers[i]['city'].toLowerCase().replace(/[^A-Z0-9]+/ig, \" \").includes(input.toLowerCase())) {
          markers[i].setVisible(true)
        }else{
          markers[i].setVisible(false)
        }
      }
    }

    //cache tous les markers et fait apparaitre seulement ceux passés en paramètre
    function show(markersVisibles){
      for (var i = 0; i < markers.length; i++){
        markers[i].setVisible(false)
      }
      for (var i = 0; i < markersVisibles.length; i++){
        markersVisibles[i].setVisible(true)
      }
    }
    </script>
  </body>
</html>
{% endblock %}", "matete/carte.html.twig", "/home/jules/matete/website/templates/matete/carte.html.twig");
    }
}

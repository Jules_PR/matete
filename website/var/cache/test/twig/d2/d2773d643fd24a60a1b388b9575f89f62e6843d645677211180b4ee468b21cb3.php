<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/gestionUsers.html.twig */
class __TwigTemplate_3fe8a24b6944c2accdab6340d885f0d9dd62fcf70c92c9bece7054c731e66b16 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/gestionUsers.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/gestionUsers.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/gestionUsers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1 class='mt-2 center'>Liste des utilistateurs</h1>
<div class='center mb-3'>
<input type=\"radio\" id=\"Tous\" name=\"drone\" value=\"Tous\" onclick=\"onClick(this);\" checked>
<label for=\"Tous\">Tous</label>
<input class='ml-3' type=\"radio\" id=\"Admins\" name=\"drone\" onclick=\"onClick(this);\" value=\"Admins\">
<label for=\"Admins\">Admins</label>
<input class='ml-3' type=\"radio\" id=\"Suspendus\" name=\"drone\" onclick=\"onClick(this);\" value=\"Suspendus\">
<label for=\"Suspendus\">Suspendus</label>
</div>

<div class='container'>
<div class='row'>
";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 16, $this->source); })())));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 17
            echo "    ";
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["user"], "roles", [], "any", false, false, false, 17), 0, [], "array", false, false, false, 17), "ROLE_ADMIN"))) {
                // line 18
                echo "        <div class='col-md-3 mb-4 Tous Admins'>
        <div class=\"card border-info h-100\" style=\"max-width: 20rem;\">
        <div class=\"card-header\">Utilisateur n°";
                // line 20
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 20), "html", null, true);
                echo "</div>
        <div class=\"card-body\">
            <h4 class=\"card-title\">";
                // line 22
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 22), "html", null, true);
                echo "</h4>
            <hr>
            <p class=\"card-text\"> ";
                // line 24
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 24), "html", null, true);
                echo " </p>
            <hr>
            <p class='card-text'> ";
                // line 26
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "numTel", [], "any", false, false, false, 26), "html", null, true);
                echo "</p>
            <hr>
            <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger\" href=\"";
                // line 28
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 28)]), "html", null, true);
                echo "\">Supprimer</a>
        </div>
        </div>
        </div>
    ";
            } else {
                // line 33
                echo "        ";
                if (twig_get_attribute($this->env, $this->source, $context["user"], "suspendu", [], "any", false, false, false, 33)) {
                    // line 34
                    echo "        <div class='col-md-3 mb-4 Tous Suspendus'>
        <div class=\"card border-warning\" style=\"max-width: 20rem;\">
        <div class=\"card-header\">Utilisateur n°";
                    // line 36
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 36), "html", null, true);
                    echo "</div>
        <div class=\"card-body\">
            <h4 class=\"card-title\">";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 38), "html", null, true);
                    echo "</h4>
            <hr>
            <p class=\"card-text\"> ";
                    // line 40
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 40), "html", null, true);
                    echo " </p>
            <hr>
            <p class='card-text'> ";
                    // line 42
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "numTel", [], "any", false, false, false, 42), "html", null, true);
                    echo "</p>
            <hr>
            <a class=\"btn btn-outline-warning\" href=\"";
                    // line 44
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_suspend", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 44)]), "html", null, true);
                    echo "\">Réactiver</a>
            <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger mt-2\" href=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 45)]), "html", null, true);
                    echo "\">Supprimer</a>
        </div>
        </div>
        </div>
        ";
                } else {
                    // line 50
                    echo "        <div class='col-md-3 mb-4 Tous'>
        <div class=\"card bg-light\" style=\"max-width: 20rem;\">
        <div class=\"card-header\">Utilisateur n°";
                    // line 52
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 52), "html", null, true);
                    echo "</div>
        <div class=\"card-body\">
            <h4 class=\"card-title\">";
                    // line 54
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 54), "html", null, true);
                    echo "</h4>
            <hr>
            <p class=\"card-text\"> ";
                    // line 56
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 56), "html", null, true);
                    echo " </p>
            <hr>
            <p class='card-text'> ";
                    // line 58
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "numTel", [], "any", false, false, false, 58), "html", null, true);
                    echo "</p>
            <hr>
            <a class=\"btn btn-outline-warning\" href=\"";
                    // line 60
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_suspend", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 60)]), "html", null, true);
                    echo "\">Suspendre</a>
            <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger mt-2\" href=\"";
                    // line 61
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 61)]), "html", null, true);
                    echo "\">Supprimer</a>
        </div>
        </div>
        </div>
        ";
                }
                // line 66
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "</div>
</div>

<script>
function onClick(myRadio) {
    //masquer ceux qui ont la classe tous
    var tous = document.getElementsByClassName(\"Tous\");
    for (var i = 0; i < tous.length; i++) {
    tous[i].style.display=\"none\";
    }

    //afficher ceux qui ont la classe en param
    var items = document.getElementsByClassName(myRadio.value);
    for (var i = 0; i < items.length; i++) {
    items[i].style.display=\"block\";
    }
}
</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/gestionUsers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 68,  196 => 66,  188 => 61,  184 => 60,  179 => 58,  174 => 56,  169 => 54,  164 => 52,  160 => 50,  152 => 45,  148 => 44,  143 => 42,  138 => 40,  133 => 38,  128 => 36,  124 => 34,  121 => 33,  113 => 28,  108 => 26,  103 => 24,  98 => 22,  93 => 20,  89 => 18,  86 => 17,  82 => 16,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<h1 class='mt-2 center'>Liste des utilistateurs</h1>
<div class='center mb-3'>
<input type=\"radio\" id=\"Tous\" name=\"drone\" value=\"Tous\" onclick=\"onClick(this);\" checked>
<label for=\"Tous\">Tous</label>
<input class='ml-3' type=\"radio\" id=\"Admins\" name=\"drone\" onclick=\"onClick(this);\" value=\"Admins\">
<label for=\"Admins\">Admins</label>
<input class='ml-3' type=\"radio\" id=\"Suspendus\" name=\"drone\" onclick=\"onClick(this);\" value=\"Suspendus\">
<label for=\"Suspendus\">Suspendus</label>
</div>

<div class='container'>
<div class='row'>
{% for user in users|reverse %}
    {% if user.roles[0] == \"ROLE_ADMIN\" %}
        <div class='col-md-3 mb-4 Tous Admins'>
        <div class=\"card border-info h-100\" style=\"max-width: 20rem;\">
        <div class=\"card-header\">Utilisateur n°{{ user.id }}</div>
        <div class=\"card-body\">
            <h4 class=\"card-title\">{{ user.username }}</h4>
            <hr>
            <p class=\"card-text\"> {{ user.email }} </p>
            <hr>
            <p class='card-text'> {{ user.numTel }}</p>
            <hr>
            <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger\" href=\"{{ path('user_delete', {'id': user.id}) }}\">Supprimer</a>
        </div>
        </div>
        </div>
    {% else %}
        {% if user.suspendu %}
        <div class='col-md-3 mb-4 Tous Suspendus'>
        <div class=\"card border-warning\" style=\"max-width: 20rem;\">
        <div class=\"card-header\">Utilisateur n°{{ user.id }}</div>
        <div class=\"card-body\">
            <h4 class=\"card-title\">{{ user.username }}</h4>
            <hr>
            <p class=\"card-text\"> {{ user.email }} </p>
            <hr>
            <p class='card-text'> {{ user.numTel }}</p>
            <hr>
            <a class=\"btn btn-outline-warning\" href=\"{{ path('user_suspend', {'id': user.id}) }}\">Réactiver</a>
            <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger mt-2\" href=\"{{ path('user_delete', {'id': user.id}) }}\">Supprimer</a>
        </div>
        </div>
        </div>
        {% else %}
        <div class='col-md-3 mb-4 Tous'>
        <div class=\"card bg-light\" style=\"max-width: 20rem;\">
        <div class=\"card-header\">Utilisateur n°{{ user.id }}</div>
        <div class=\"card-body\">
            <h4 class=\"card-title\">{{ user.username }}</h4>
            <hr>
            <p class=\"card-text\"> {{ user.email }} </p>
            <hr>
            <p class='card-text'> {{ user.numTel }}</p>
            <hr>
            <a class=\"btn btn-outline-warning\" href=\"{{ path('user_suspend', {'id': user.id}) }}\">Suspendre</a>
            <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger mt-2\" href=\"{{ path('user_delete', {'id': user.id}) }}\">Supprimer</a>
        </div>
        </div>
        </div>
        {% endif %}
    {% endif %}
{% endfor %}
</div>
</div>

<script>
function onClick(myRadio) {
    //masquer ceux qui ont la classe tous
    var tous = document.getElementsByClassName(\"Tous\");
    for (var i = 0; i < tous.length; i++) {
    tous[i].style.display=\"none\";
    }

    //afficher ceux qui ont la classe en param
    var items = document.getElementsByClassName(myRadio.value);
    for (var i = 0; i < items.length; i++) {
    items[i].style.display=\"block\";
    }
}
</script>

{% endblock %}", "admin/gestionUsers.html.twig", "/home/jules/matete/website/templates/admin/gestionUsers.html.twig");
    }
}

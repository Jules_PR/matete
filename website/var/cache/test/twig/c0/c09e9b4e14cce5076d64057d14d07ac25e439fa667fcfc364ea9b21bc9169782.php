<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/showProfil.html.twig */
class __TwigTemplate_bc7b8783ed01c37090d5219fee7938dd86f714ceacddc85aae289adb7bbc1c60 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/showProfil.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/showProfil.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security/showProfil.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1 class='center mt-2'style=\"margin-bottom:3%;\">Votre profil de vendeur</h1>
<div class=\"container\">
<div class='row'>
<div class='col-8'>
                <h4>Nom d'utilisateur :</h4>
                <p class='mb-3'>";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 9, $this->source); })()), "username", [], "any", false, false, false, 9), "html", null, true);
        echo "</p>
                <h4>Email :</h4>
                <p class='mb-3'>";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 11, $this->source); })()), "email", [], "any", false, false, false, 11), "html", null, true);
        echo "</p>
                <h4>Numéro de tel :</h4>
                <p class='mb-3'>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 13, $this->source); })()), "numTel", [], "any", false, false, false, 13), "html", null, true);
        echo "</p>
                <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger mb-3\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "user", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14)]), "html", null, true);
        echo "\">Supprimer le compte</a>
</div>
";
        // line 16
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "user", [], "any", false, false, false, 16), "suspendu", [], "any", false, false, false, 16), false))) {
            // line 17
            echo "    <div class='col'>
    ";
            // line 18
            if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["emplacements"]) || array_key_exists("emplacements", $context) ? $context["emplacements"] : (function () { throw new RuntimeError('Variable "emplacements" does not exist.', 18, $this->source); })())), 0))) {
                // line 19
                echo "        <h4>Vos emplacements :</h4>
        ";
                // line 20
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["emplacements"]) || array_key_exists("emplacements", $context) ? $context["emplacements"] : (function () { throw new RuntimeError('Variable "emplacements" does not exist.', 20, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["emplacement"]) {
                    // line 21
                    echo "            <h6 class=\"m-1\">-";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emplacement"], "adresse", [], "any", false, false, false, 21), "html", null, true);
                    echo " (";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["emplacement"], "codePostal", [], "any", false, false, false, 21), "html", null, true);
                    echo ") <a class=\"text-danger\" onclick=\"return confirm('Êtes-vous sûr? Cela supprimera également vos annonces utilisant cet emplacement')\" href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("emplacement_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["emplacement"], "id", [], "any", false, false, false, 21)]), "html", null, true);
                    echo "\"><i onmouseover=\"hover(this);\" onmouseleave=\"noHover(this);\" class='uil uil-times'></i></a></h6>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['emplacement'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "    ";
            } else {
                // line 24
                echo "        <h4>Vous n'avez pas d'emplacements</h4>
    ";
            }
            // line 26
            echo "
";
            // line 27
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), 'form_start', ["attr" => ["novalidate" => "novalidate"]]);
            echo "
<label class=\"mt-3 h6\">Ajouter un emplacement:</label><br>

";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 30, $this->source); })()), "adresse", [], "any", false, false, false, 30), 'widget');
            echo "
<div class='text-danger form_error'>";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "adresse", [], "any", false, false, false, 31), 'errors');
            echo "</div>

";
            // line 33
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "codePostal", [], "any", false, false, false, 33), 'widget');
            echo "
<div class='text-danger form_error'>";
            // line 34
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 34, $this->source); })()), "codePostal", [], "any", false, false, false, 34), 'errors');
            echo "</div>
             
<button type=\"submit\" class=\"btn btn-outline-success center mt-4\">Ajouter l'emplacement</button>
";
            // line 37
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 37, $this->source); })()), 'form_end');
            echo "
</div>

<div class='row mt-5'>
    <div class=\"container\">
    ";
            // line 42
            if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 42, $this->source); })())), 0))) {
                // line 43
                echo "        <h4>Vos annonces :</h4>
        <div class=\"row\">
        ";
                // line 45
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 45, $this->source); })())));
                foreach ($context['_seq'] as $context["_key"] => $context["annonce"]) {
                    // line 46
                    echo "        <div class=\"col-md-4 mt-2\">
        <div class=\"card h-60 mb-1\">
        <div class='card-body'>
            <h5 class='card-title'><strong>";
                    // line 49
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "nom", [], "any", false, false, false, 49), "html", null, true);
                    echo "</strong></h5>
            <img class='center' src='";
                    // line 50
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "image", [], "any", false, false, false, 50), "html", null, true);
                    echo "' alt=\"Image de l'annonce\" width='95%' style='border-radius:6px'>
            <div class=\"card-body\">
            <a href=\"";
                    // line 52
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_show", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 52)]), "html", null, true);
                    echo "\" class='btn btn-outline-dark p-2'>Voir plus</a>
            <a href=\"";
                    // line 53
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 53)]), "html", null, true);
                    echo "\" class='btn btn-outline-info p-2'>Modifier</a>
            </div>
        </div>
        </div>
        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annonce'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "        </div>
    ";
            } else {
                // line 61
                echo "        <h4 class='h4 mt-1'>Vous n'avez pas d'annonces en ligne. <a href='";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_new");
                echo "' class='text-primary' style='cursor:pointer'><i class='uil uil-plus-circle'></i></a> </h4>
    ";
            }
            // line 63
            echo "    </div>
</div>
";
        } else {
            // line 66
            echo "    <h1 style=\"margin-top:5%;\"><font color=\"red\"> Votre compte a été bloqué </font></h1>
";
        }
        // line 68
        echo "<script>
function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/showProfil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 68,  219 => 66,  214 => 63,  208 => 61,  204 => 59,  192 => 53,  188 => 52,  183 => 50,  179 => 49,  174 => 46,  170 => 45,  166 => 43,  164 => 42,  156 => 37,  150 => 34,  146 => 33,  141 => 31,  137 => 30,  131 => 27,  128 => 26,  124 => 24,  121 => 23,  108 => 21,  104 => 20,  101 => 19,  99 => 18,  96 => 17,  94 => 16,  89 => 14,  85 => 13,  80 => 11,  75 => 9,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<h1 class='center mt-2'style=\"margin-bottom:3%;\">Votre profil de vendeur</h1>
<div class=\"container\">
<div class='row'>
<div class='col-8'>
                <h4>Nom d'utilisateur :</h4>
                <p class='mb-3'>{{user.username}}</p>
                <h4>Email :</h4>
                <p class='mb-3'>{{user.email}}</p>
                <h4>Numéro de tel :</h4>
                <p class='mb-3'>{{user.numTel}}</p>
                <a onclick=\"return confirm('Êtes-vous sûr?')\" class=\"btn btn-outline-danger mb-3\" href=\"{{ path('profil_delete', {'id': app.user.id}) }}\">Supprimer le compte</a>
</div>
{% if app.user.suspendu == false %}
    <div class='col'>
    {% if emplacements|length > 0 %}
        <h4>Vos emplacements :</h4>
        {% for emplacement in emplacements %}
            <h6 class=\"m-1\">-{{emplacement.adresse}} ({{emplacement.codePostal}}) <a class=\"text-danger\" onclick=\"return confirm('Êtes-vous sûr? Cela supprimera également vos annonces utilisant cet emplacement')\" href=\"{{ path('emplacement_delete', {'id' : emplacement.id}) }}\"><i onmouseover=\"hover(this);\" onmouseleave=\"noHover(this);\" class='uil uil-times'></i></a></h6>
        {% endfor %}
    {% else %}
        <h4>Vous n'avez pas d'emplacements</h4>
    {% endif %}

{{ form_start(form, {attr: {novalidate: 'novalidate'}}) }}
<label class=\"mt-3 h6\">Ajouter un emplacement:</label><br>

{{ form_widget(form.adresse) }}
<div class='text-danger form_error'>{{ form_errors(form.adresse) }}</div>

{{ form_widget(form.codePostal) }}
<div class='text-danger form_error'>{{ form_errors(form.codePostal) }}</div>
             
<button type=\"submit\" class=\"btn btn-outline-success center mt-4\">Ajouter l'emplacement</button>
{{ form_end(form) }}
</div>

<div class='row mt-5'>
    <div class=\"container\">
    {% if annonces|length > 0 %}
        <h4>Vos annonces :</h4>
        <div class=\"row\">
        {% for annonce in annonces|reverse %}
        <div class=\"col-md-4 mt-2\">
        <div class=\"card h-60 mb-1\">
        <div class='card-body'>
            <h5 class='card-title'><strong>{{annonce.nom}}</strong></h5>
            <img class='center' src='{{annonce.image}}' alt=\"Image de l'annonce\" width='95%' style='border-radius:6px'>
            <div class=\"card-body\">
            <a href=\"{{ path('annonce_show', {'id': annonce.id}) }}\" class='btn btn-outline-dark p-2'>Voir plus</a>
            <a href=\"{{ path('annonce_edit', {'id': annonce.id}) }}\" class='btn btn-outline-info p-2'>Modifier</a>
            </div>
        </div>
        </div>
        </div>
        {% endfor %}
        </div>
    {% else %}
        <h4 class='h4 mt-1'>Vous n'avez pas d'annonces en ligne. <a href='{{ path('annonce_new') }}' class='text-primary' style='cursor:pointer'><i class='uil uil-plus-circle'></i></a> </h4>
    {% endif %}
    </div>
</div>
{% else %}
    <h1 style=\"margin-top:5%;\"><font color=\"red\"> Votre compte a été bloqué </font></h1>
{% endif %}
<script>
function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}
</script>
{% endblock %}
", "security/showProfil.html.twig", "/home/jules/matete/website/templates/security/showProfil.html.twig");
    }
}

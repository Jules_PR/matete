<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/show.html.twig */
class __TwigTemplate_def706ccd57174c8cd45f8a92597d311d8db51aea9f90c8cc51bffee35428f32 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/show.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"card center\" style='width:20%;margin-top:2%;align-items:center'>
  <div class=\"card-body\">
    <h5 class=\"card-title mt-2\"><strong>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 6, $this->source); })()), "nom", [], "any", false, false, false, 6), "html", null, true);
        echo "</strong></h5>
    <h6 class=\"card-subtitle text-muted\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 7, $this->source); })()), "leUser", [], "any", false, false, false, 7), "username", [], "any", false, false, false, 7), "html", null, true);
        echo "</h6>
  </div>
  <img src='";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 9, $this->source); })()), "image", [], "any", false, false, false, 9), "html", null, true);
        echo "' alt=\"Image de l'annonce\" width='95%' style='border-radius:6px'>
  <div class=\"card-body\">
    <p class=\"card-text\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 11, $this->source); })()), "description", [], "any", false, false, false, 11), "html", null, true);
        echo "</p>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">Catégorie : ";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 14, $this->source); })()), "laCategorie", [], "any", false, false, false, 14), "nom", [], "any", false, false, false, 14), "html", null, true);
        echo "</li>
    <li class=\"list-group-item\">Quantité : ";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 15, $this->source); })()), "quantite", [], "any", false, false, false, 15), "html", null, true);
        echo " kg</li>
    <li class=\"list-group-item\">Adresse email de ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 16, $this->source); })()), "leUser", [], "any", false, false, false, 16), "username", [], "any", false, false, false, 16), "html", null, true);
        echo " : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 16, $this->source); })()), "leUser", [], "any", false, false, false, 16), "email", [], "any", false, false, false, 16), "html", null, true);
        echo "</li>
    <li class=\"list-group-item\">Emplacement : ";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 17, $this->source); })()), "emplacement", [], "any", false, false, false, 17), "adresse", [], "any", false, false, false, 17), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 17, $this->source); })()), "emplacement", [], "any", false, false, false, 17), "codePostal", [], "any", false, false, false, 17), "html", null, true);
        echo ")</li>
  </ul>
  <div class=\"card-body\">
    ";
        // line 20
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 20, $this->source); })()), "user", [], "any", false, false, false, 20)) {
            // line 21
            echo "    ";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 21, $this->source); })()), "user", [], "any", false, false, false, 21), "isAdmin", [], "any", false, false, false, 21)) {
                // line 22
                echo "        ";
                if ((isset($context["inPanier"]) || array_key_exists("inPanier", $context) ? $context["inPanier"] : (function () { throw new RuntimeError('Variable "inPanier" does not exist.', 22, $this->source); })())) {
                    // line 23
                    echo "            <a style='' href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 23, $this->source); })()), "id", [], "any", false, false, false, 23), "from" => "annonce_show"]), "html", null, true);
                    echo "\" class='btn btn-outline-warning mb-3'>Retirer du panier</a><br>
        ";
                } else {
                    // line 25
                    echo "            <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajouter_panier", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 25, $this->source); })()), "id", [], "any", false, false, false, 25), "from" => "annonce_show"]), "html", null, true);
                    echo "\" class='btn btn-outline-primary mb-3'><i class='uil uil-shopping-cart'></i> Ajouter au Panier</a><br>
        ";
                }
                // line 27
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_edit", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 27, $this->source); })()), "id", [], "any", false, false, false, 27)]), "html", null, true);
                echo "\" class='btn btn-outline-info mb-3'>Modifier</a><br>
        <a href=\"";
                // line 28
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 28, $this->source); })()), "id", [], "any", false, false, false, 28)]), "html", null, true);
                echo "\"  onclick=\"return confirm('Êtes-vous sûr?')\" class='btn btn-outline-danger'>Supprimer</a><br>
    ";
            } else {
                // line 30
                echo "            ";
                if (twig_in_filter((isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 30, $this->source); })()), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 30, $this->source); })()), "user", [], "any", false, false, false, 30), "lesAnnonces", [], "any", false, false, false, 30))) {
                    // line 31
                    echo "                <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_edit", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 31, $this->source); })()), "id", [], "any", false, false, false, 31)]), "html", null, true);
                    echo "\" class='btn btn-outline-info mb-3'>Modifier</a><br>
                <a href=\"";
                    // line 32
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 32, $this->source); })()), "id", [], "any", false, false, false, 32)]), "html", null, true);
                    echo "\" onclick=\"return confirm('Êtes-vous sûr?')\" class='btn btn-outline-danger'>Supprimer</a><br>
            ";
                } else {
                    // line 34
                    echo "                ";
                    if ((isset($context["inPanier"]) || array_key_exists("inPanier", $context) ? $context["inPanier"] : (function () { throw new RuntimeError('Variable "inPanier" does not exist.', 34, $this->source); })())) {
                        // line 35
                        echo "                    <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 35, $this->source); })()), "id", [], "any", false, false, false, 35), "from" => "annonce_show"]), "html", null, true);
                        echo "\" class='btn btn-outline-warning mb-3'>Retirer du panier</a><br>
                ";
                    } else {
                        // line 37
                        echo "                    <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajouter_panier", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 37, $this->source); })()), "id", [], "any", false, false, false, 37), "from" => "annonce_show"]), "html", null, true);
                        echo "\" class='btn btn-outline-primary mb-3'><i class='uil uil-shopping-cart'></i>Ajouter au Panier</a><br>
                ";
                    }
                    // line 39
                    echo "            ";
                }
                // line 40
                echo "    ";
            }
        } else {
            // line 42
            if ((isset($context["inPanier"]) || array_key_exists("inPanier", $context) ? $context["inPanier"] : (function () { throw new RuntimeError('Variable "inPanier" does not exist.', 42, $this->source); })())) {
                // line 43
                echo "    <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 43, $this->source); })()), "id", [], "any", false, false, false, 43), "from" => "annonce_show"]), "html", null, true);
                echo "\" class='btn btn-outline-warning mb-3'>Retirer du panier</a><br>
";
            } else {
                // line 45
                echo "    <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajouter_panier", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 45, $this->source); })()), "id", [], "any", false, false, false, 45), "from" => "annonce_show"]), "html", null, true);
                echo "\" class='btn btn-outline-primary mb-3'><i class='uil uil-shopping-cart'></i> Ajouter au Panier</a><br>
";
            }
        }
        // line 48
        echo "  </div>
</div>
</div>
<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;
    
    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}

function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 48,  187 => 45,  181 => 43,  179 => 42,  175 => 40,  172 => 39,  166 => 37,  160 => 35,  157 => 34,  152 => 32,  147 => 31,  144 => 30,  139 => 28,  134 => 27,  128 => 25,  122 => 23,  119 => 22,  116 => 21,  114 => 20,  106 => 17,  100 => 16,  96 => 15,  92 => 14,  86 => 11,  81 => 9,  76 => 7,  72 => 6,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"card center\" style='width:20%;margin-top:2%;align-items:center'>
  <div class=\"card-body\">
    <h5 class=\"card-title mt-2\"><strong>{{annonce.nom}}</strong></h5>
    <h6 class=\"card-subtitle text-muted\">{{annonce.leUser.username}}</h6>
  </div>
  <img src='{{annonce.image}}' alt=\"Image de l'annonce\" width='95%' style='border-radius:6px'>
  <div class=\"card-body\">
    <p class=\"card-text\">{{annonce.description}}</p>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">Catégorie : {{annonce.laCategorie.nom}}</li>
    <li class=\"list-group-item\">Quantité : {{annonce.quantite}} kg</li>
    <li class=\"list-group-item\">Adresse email de {{annonce.leUser.username}} : {{annonce.leUser.email}}</li>
    <li class=\"list-group-item\">Emplacement : {{annonce.emplacement.adresse}} ({{annonce.emplacement.codePostal}})</li>
  </ul>
  <div class=\"card-body\">
    {% if app.user %}
    {% if app.user.isAdmin %}
        {% if inPanier %}
            <a style='' href=\"{{ path('panier_delete', {'id': annonce.id, 'from': 'annonce_show'}) }}\" class='btn btn-outline-warning mb-3'>Retirer du panier</a><br>
        {% else %}
            <a href=\"{{ path('ajouter_panier', {'id': annonce.id, 'from': 'annonce_show'}) }}\" class='btn btn-outline-primary mb-3'><i class='uil uil-shopping-cart'></i> Ajouter au Panier</a><br>
        {% endif %}
        <a href=\"{{ path('annonce_edit', {'id': annonce.id}) }}\" class='btn btn-outline-info mb-3'>Modifier</a><br>
        <a href=\"{{ path('annonce_delete', {'id': annonce.id}) }}\"  onclick=\"return confirm('Êtes-vous sûr?')\" class='btn btn-outline-danger'>Supprimer</a><br>
    {% else %}
            {% if annonce in app.user.lesAnnonces %}
                <a href=\"{{ path('annonce_edit', {'id': annonce.id}) }}\" class='btn btn-outline-info mb-3'>Modifier</a><br>
                <a href=\"{{ path('annonce_delete', {'id': annonce.id}) }}\" onclick=\"return confirm('Êtes-vous sûr?')\" class='btn btn-outline-danger'>Supprimer</a><br>
            {% else %}
                {% if inPanier %}
                    <a href=\"{{ path('panier_delete', {'id': annonce.id, 'from': 'annonce_show'}) }}\" class='btn btn-outline-warning mb-3'>Retirer du panier</a><br>
                {% else %}
                    <a href=\"{{ path('ajouter_panier', {'id': annonce.id, 'from': 'annonce_show'}) }}\" class='btn btn-outline-primary mb-3'><i class='uil uil-shopping-cart'></i>Ajouter au Panier</a><br>
                {% endif %}
            {% endif %}
    {% endif %}
{% else %}
{% if inPanier %}
    <a href=\"{{ path('panier_delete', {'id': annonce.id, 'from': 'annonce_show'}) }}\" class='btn btn-outline-warning mb-3'>Retirer du panier</a><br>
{% else %}
    <a href=\"{{ path('ajouter_panier', {'id': annonce.id, 'from': 'annonce_show'}) }}\" class='btn btn-outline-primary mb-3'><i class='uil uil-shopping-cart'></i> Ajouter au Panier</a><br>
{% endif %}
{% endif %}
  </div>
</div>
</div>
<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;
    
    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}

function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}
</script>
{% endblock %}", "matete/show.html.twig", "/home/jules/matete/website/templates/matete/show.html.twig");
    }
}

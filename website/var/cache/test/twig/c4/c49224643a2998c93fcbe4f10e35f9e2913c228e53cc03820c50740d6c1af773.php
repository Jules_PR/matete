<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/index.html.twig */
class __TwigTemplate_642dc355413c239e3f7e487f1af16944d3d6bb2eac30a387c4a581355c573cda extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1 class='center mt-2'>Les Annonces</h1>
<select class='form-control mb-3' style=\"width:10%;position:relative;left:45%\" onchange=\"changeCategorie(this)\">
<option value='Tout'>Toutes catégories</option>
";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 7, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
            // line 8
            echo "<option value='";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categorie"], "nom", [], "any", false, false, false, 8), "html", null, true);
            echo "'>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categorie"], "nom", [], "any", false, false, false, 8), "html", null, true);
            echo "</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</select>
";
        // line 11
        if ((-1 === twig_compare(twig_length_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 11, $this->source); })())), 1))) {
            // line 12
            echo "    <h4 class='center mt-2'>Il n'y a pas d'annonces pour le moment.</h4>
";
        } else {
            // line 14
            echo "    <div class=\"container\">
    <div class=\"row\">
    ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 16, $this->source); })())));
            foreach ($context['_seq'] as $context["_key"] => $context["annonce"]) {
                // line 17
                echo "        <div class=\"col-md-4 mt-3 Tout ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "laCategorie", [], "any", false, false, false, 17), "nom", [], "any", false, false, false, 17), "html", null, true);
                echo "\" id='";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 17), "html", null, true);
                echo "'>
        <div class=\"card mb-3 h-100\" id='";
                // line 18
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 18), "html", null, true);
                echo "'>
            <div class=\"card-body\">
                <h5 class=\"card-title\"><strong>";
                // line 20
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "nom", [], "any", false, false, false, 20), "html", null, true);
                echo "</strong></h5>
                <h6 class=\"card-subtitle text-muted\">de ";
                // line 21
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "leUser", [], "any", false, false, false, 21), "username", [], "any", false, false, false, 21), "html", null, true);
                echo "</h6>
                ";
                // line 22
                if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 22, $this->source); })()), "user", [], "any", false, false, false, 22) && twig_in_filter($context["annonce"], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 22, $this->source); })()), "user", [], "any", false, false, false, 22), "lesAnnonces", [], "any", false, false, false, 22)))) {
                    // line 23
                    echo "                    <h5 class='badge bg-info' style='position:absolute; color:white; left:5.5%; top:13%'>Votre annonce</h5>
                ";
                } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,                 // line 24
$context["annonce"], "id", [], "any", false, false, false, 24), twig_get_array_keys_filter((isset($context["panier"]) || array_key_exists("panier", $context) ? $context["panier"] : (function () { throw new RuntimeError('Variable "panier" does not exist.', 24, $this->source); })())))) {
                    // line 25
                    echo "                    <!--badge panier-->
                    <h5 class='badge bg-success' style='position:absolute; color:white; left:5.5%; top:13%'><a class='simple-link' href=\"";
                    // line 26
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 26), "from" => "lesAnnonces"]), "html", null, true);
                    echo "\"><i class='uil uil-times times' style='font-size:13px' onmouseover=\"hover(this);\" onmouseleave=\"noHover(this);\"></i></a> <a href='/panier' class='simple-link'>Dans votre panier</a></h5>
                ";
                }
                // line 28
                echo "            </div>
            <img class='center' src=\"";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "image", [], "any", false, false, false, 29), "html", null, true);
                echo "\" height='250' width='95%' alt=\"Image de l'annonce\" style='border-radius:6px'>
            <div class=\"card-body\">
                <p class=\"card-text\">";
                // line 31
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "description", [], "any", false, false, false, 31), "html", null, true);
                echo "</p>
                <hr class=\"mt-2\">
                <a href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_show", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 33)]), "html", null, true);
                echo "\" class='btn btn-outline-dark p-2' style='margin-top:3.5%'>Voir plus</a>
                ";
                // line 34
                if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 34, $this->source); })()), "user", [], "any", false, false, false, 34)) {
                    // line 35
                    echo "                    ";
                    if ((!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 35), twig_get_array_keys_filter((isset($context["panier"]) || array_key_exists("panier", $context) ? $context["panier"] : (function () { throw new RuntimeError('Variable "panier" does not exist.', 35, $this->source); })()))) && !twig_in_filter($context["annonce"], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 35, $this->source); })()), "user", [], "any", false, false, false, 35), "lesAnnonces", [], "any", false, false, false, 35)))) {
                        // line 36
                        echo "                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajouter_panier", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 36), "from" => "lesAnnonces"]), "html", null, true);
                        echo "\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    ";
                    }
                    // line 38
                    echo "                    ";
                    if (twig_in_filter($context["annonce"], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 38, $this->source); })()), "user", [], "any", false, false, false, 38), "lesAnnonces", [], "any", false, false, false, 38))) {
                        // line 39
                        echo "                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                        echo "\" class='btn btn-outline-info p-2' style='margin-top:3.5%'>Modifier</a>
                    ";
                    }
                    // line 41
                    echo "                ";
                } else {
                    // line 42
                    echo "                    ";
                    if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 42), twig_get_array_keys_filter((isset($context["panier"]) || array_key_exists("panier", $context) ? $context["panier"] : (function () { throw new RuntimeError('Variable "panier" does not exist.', 42, $this->source); })())))) {
                        // line 43
                        echo "                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajouter_panier", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 43), "from" => "lesAnnonces"]), "html", null, true);
                        echo "\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    ";
                    }
                    // line 45
                    echo "                ";
                }
                // line 46
                echo "            </div>
             <div class='card-footer'>
                <small class=\"text-muted\">";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "createdAt", [], "any", false, false, false, 48), "d-m-Y"), "html", null, true);
                echo "</small>
            </div>
            </div>
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annonce'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "    </div>
    </div>
";
        }
        // line 56
        echo "
<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;
    
    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}

function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 56,  207 => 53,  196 => 48,  192 => 46,  189 => 45,  183 => 43,  180 => 42,  177 => 41,  171 => 39,  168 => 38,  162 => 36,  159 => 35,  157 => 34,  153 => 33,  148 => 31,  143 => 29,  140 => 28,  135 => 26,  132 => 25,  130 => 24,  127 => 23,  125 => 22,  121 => 21,  117 => 20,  112 => 18,  105 => 17,  101 => 16,  97 => 14,  93 => 12,  91 => 11,  88 => 10,  77 => 8,  73 => 7,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<h1 class='center mt-2'>Les Annonces</h1>
<select class='form-control mb-3' style=\"width:10%;position:relative;left:45%\" onchange=\"changeCategorie(this)\">
<option value='Tout'>Toutes catégories</option>
{% for categorie in categories %}
<option value='{{categorie.nom}}'>{{categorie.nom}}</option>
{% endfor %}
</select>
{% if annonces|length < 1 %}
    <h4 class='center mt-2'>Il n'y a pas d'annonces pour le moment.</h4>
{% else %}
    <div class=\"container\">
    <div class=\"row\">
    {% for annonce in annonces|reverse %}
        <div class=\"col-md-4 mt-3 Tout {{annonce.laCategorie.nom}}\" id='{{annonce.id}}'>
        <div class=\"card mb-3 h-100\" id='{{annonce.id}}'>
            <div class=\"card-body\">
                <h5 class=\"card-title\"><strong>{{annonce.nom}}</strong></h5>
                <h6 class=\"card-subtitle text-muted\">de {{annonce.leUser.username}}</h6>
                {% if app.user and annonce in app.user.lesAnnonces %}
                    <h5 class='badge bg-info' style='position:absolute; color:white; left:5.5%; top:13%'>Votre annonce</h5>
                {% elseif annonce.id in panier|keys %}
                    <!--badge panier-->
                    <h5 class='badge bg-success' style='position:absolute; color:white; left:5.5%; top:13%'><a class='simple-link' href=\"{{ path('panier_delete', {'id': annonce.id, 'from': 'lesAnnonces'}) }}\"><i class='uil uil-times times' style='font-size:13px' onmouseover=\"hover(this);\" onmouseleave=\"noHover(this);\"></i></a> <a href='/panier' class='simple-link'>Dans votre panier</a></h5>
                {% endif %}
            </div>
            <img class='center' src=\"{{annonce.image}}\" height='250' width='95%' alt=\"Image de l'annonce\" style='border-radius:6px'>
            <div class=\"card-body\">
                <p class=\"card-text\">{{annonce.description}}</p>
                <hr class=\"mt-2\">
                <a href=\"{{ path('annonce_show', {'id': annonce.id}) }}\" class='btn btn-outline-dark p-2' style='margin-top:3.5%'>Voir plus</a>
                {% if app.user %}
                    {% if annonce.id not in panier|keys and annonce not in app.user.lesAnnonces %}
                        <a href=\"{{ path('ajouter_panier', {'id': annonce.id, 'from': 'lesAnnonces'}) }}\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    {% endif %}
                    {% if annonce in app.user.lesAnnonces %}
                        <a href=\"{{ path('annonce_edit', {'id': annonce.id}) }}\" class='btn btn-outline-info p-2' style='margin-top:3.5%'>Modifier</a>
                    {% endif %}
                {% else %}
                    {% if annonce.id not in panier|keys %}
                        <a href=\"{{ path('ajouter_panier', {'id': annonce.id, 'from': 'lesAnnonces'}) }}\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    {% endif %}
                {% endif %}
            </div>
             <div class='card-footer'>
                <small class=\"text-muted\">{{annonce.createdAt|date('d-m-Y')}}</small>
            </div>
            </div>
        </div>
    {% endfor %}
    </div>
    </div>
{% endif %}

<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;
    
    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}

function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}
</script>
{% endblock %}", "matete/index.html.twig", "/home/jules/matete/website/templates/matete/index.html.twig");
    }
}

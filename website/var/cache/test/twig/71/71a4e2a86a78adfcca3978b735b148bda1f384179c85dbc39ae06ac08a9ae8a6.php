<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_6d96baa913965ab0cc2d7c1aa01ef7219aba1f5392a4032f64d1f72eaa762790 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>

<style>
body{
    font-family: 'Poppins', sans-serif;
}

.center {
  margin-left: auto;
  margin-right: auto;
  text-align:center;
}

td{
  color: red;
}

.uil{
  font-size: 1.2rem;
}

textarea {
  width: 100%;
  height: 150px;
  box-sizing: border-box;
  resize: none;
}

#map {
  height: 95vh;
  width: 100%;
  z-index:0;
}

.carré{
  background-color: #fafafa;
  max-width: 400px;
  max-height: 50px;
  left: 40%;
  padding: 8px;
  width: 90%;
  height: 90%;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  border-radius: 8px;
  z-index:1;
  position:absolute;
}

.simple-link{
  cursor:pointer;
  color:white;
}

.simple-link:hover{
  cursor:pointer;
  color:white;
  text-decoration:none;
}

table, tr, td{
  border-collapse:collapse;
}

@media screen and (max-width: 990px){
  .btn-navbar{
    width:20%;
  }
}

ul{
   list-style-type: none;
}

.form_error{
  left:-10%;
  position:relative;
  text-align:left;
}

/*
.row{
  border: 1px solid black;
}

.col{
  border: 1px solid black;
}*/
</style>

<html>
    <head>
        <meta charset=\"UTF-8\">
        ";
        // line 93
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 100
        echo "
        ";
        // line 101
        $this->displayBlock('javascripts', $context, $blocks);
        // line 105
        echo "    </head>
    <body>
    ";
        // line 107
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 107, $this->source); })()), "user", [], "any", false, false, false, 107) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 107, $this->source); })()), "user", [], "any", false, false, false, 107), "isAdmin", [], "any", false, false, false, 107))) {
            // line 108
            echo "    <nav class=\"navbar nav-tabs navbar-expand-lg navbar-dark bg-info\">
    ";
        } else {
            // line 110
            echo "    <nav class=\"navbar nav-tabs navbar-expand-lg navbar-dark bg-primary\">
    ";
        }
        // line 112
        echo "    <div class=\"container-fluid\">
    <a class=\"navbar-brand\" href=\"/\"><img src=\"/images/logo-removebg-preview-removebg-preview_1.png\" width=\"45\" /><strong style='font-family: \"Oooh Baby\", cursive;font-size:30px'>Matete</strong></a>
    <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarColor01\" aria-controls=\"navbarColor01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
      <span class=\"navbar-toggler-icon\"></span>
    </button>
  <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">
    <ul class=\"navbar-nav\">
      <li class=\"nav-item active\">
        <a class=\"nav-link btn btn-outline-primary m-1 btn-navbar\" href=\"";
        // line 120
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("carte");
        echo "\">Carte</a>
      </li>
      <li class=\"nav-item active\">
        <a class=\"nav-link btn btn-outline-primary m-1 btn-navbar\" href=\"";
        // line 123
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lesAnnonces");
        echo "\">Les annonces</a>
      </li>
      <li class=\"nav-item active\">
        <a class=\"nav-link btn btn-outline-primary m-1 btn-navbar\" href=\"/nouveau\">Créer une annonce</a>
      </li>
      ";
        // line 128
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 128, $this->source); })()), "user", [], "any", false, false, false, 128) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 128, $this->source); })()), "user", [], "any", false, false, false, 128), "isAdmin", [], "any", false, false, false, 128))) {
            // line 129
            echo "      <li class=\"nav-item dropdown m-1\">
        <a class=\"nav-link dropdown-toggle text-light\" data-bs-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Admin</a>
        <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\" href=\"/users\">Gérer les users</a>
          <div class=\"dropdown-divider\"></div>
          <a class=\"dropdown-item\" href=\"/gestionCategorie\">Gérer les catégories</a>
          <div class=\"dropdown-divider\"></div>
          <a class=\"dropdown-item\" href=\"/statistiques\">Voir les statistiques</a>
        </div>
      </li>
      ";
        }
        // line 140
        echo "    </ul>
    <ul class=\"navbar-nav\" style=\"margin-left:auto; margin-right:30px\">
       <li class=\"nav-item active\">
        <a href=\"/panier\" class=\"nav-link btn btn-outline-primary m-1 btn-navbar\"><i class=\"uil uil-shopping-cart m-1\"></i>Panier</a>
      </li>
    ";
        // line 145
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 145, $this->source); })()), "user", [], "any", false, false, false, 145)) {
            // line 146
            echo "      <li class=\"nav-item active\">
          <a class=\"nav-link btn btn-outline-primary btn-navbar m-1\" href=\"/showProfil\"><i class=\"uil uil-user-square m-1\"></i>Votre profil</a>
      </li>
      <li class=\"nav-item active\">
          <a class=\"nav-link btn btn-outline-danger btn-navbar m-1\" href=\"/logout\"><i class=\"uil uil-user-times m-1\"></i>Se déconnecter</a>
      </li>
    ";
        } else {
            // line 153
            echo "    <li class=\"nav-item active\">
        <a href=\"/login\" class=\"nav-link btn btn-outline-primary m-1 btn-navbar\"><i class=\"uil uil-user-check m-1\"></i>Se connecter</a>
      </li>
      <li class=\"nav-item active\">
          <a class=\"nav-link btn btn-outline-success btn-navbar m-1\" href=\"/inscription\"><i class=\"uil uil-user-plus m-1\"></i>Créer un compte</a>
      </li>
    ";
        }
        // line 160
        echo "    </ul>
  </div>
  </div>
</nav>
";
        // line 164
        if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 164, $this->source); })()), "user", [], "any", false, false, false, 164) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 164, $this->source); })()), "user", [], "any", false, false, false, 164), "suspendu", [], "any", false, false, false, 164))) {
            // line 165
            echo "<h1><font color=\"red\"> VOTRE COMPTE EST SUSPENDU </h1>
<h3>Contactez le support à <ins>matete-support@gmail.com</ins></font></h3>

";
        }
        // line 169
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 171
        echo "    </body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 93
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 94
        echo "        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/minty/bootstrap.min.css\" integrity=\"sha384-H4X+4tKc7b8s4GoMrylmy2ssQYpDHoqzPa9aKXbDwPoPUA3Ra8PA5dGzijN+ePnH\" crossorigin=\"anonymous\">        <link rel=\"stylesheet\" href=\"https://unicons.iconscout.com/release/v4.0.0/css/line.css\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"https://cdn.discordapp.com/attachments/539117854247878686/912349805722877992/logo-removebg-preview.png\">
        <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
        <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
        <link href=\"https://fonts.googleapis.com/css2?family=Oooh+Baby&display=swap\" rel=\"stylesheet\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 101
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 102
        echo "        <script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js\" integrity=\"sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js\" integrity=\"sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13\" crossorigin=\"anonymous\"></script>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 169
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 170
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 170,  302 => 169,  290 => 102,  280 => 101,  265 => 94,  255 => 93,  244 => 171,  241 => 169,  235 => 165,  233 => 164,  227 => 160,  218 => 153,  209 => 146,  207 => 145,  200 => 140,  187 => 129,  185 => 128,  177 => 123,  171 => 120,  161 => 112,  157 => 110,  153 => 108,  151 => 107,  147 => 105,  145 => 101,  142 => 100,  140 => 93,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>

<style>
body{
    font-family: 'Poppins', sans-serif;
}

.center {
  margin-left: auto;
  margin-right: auto;
  text-align:center;
}

td{
  color: red;
}

.uil{
  font-size: 1.2rem;
}

textarea {
  width: 100%;
  height: 150px;
  box-sizing: border-box;
  resize: none;
}

#map {
  height: 95vh;
  width: 100%;
  z-index:0;
}

.carré{
  background-color: #fafafa;
  max-width: 400px;
  max-height: 50px;
  left: 40%;
  padding: 8px;
  width: 90%;
  height: 90%;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  border-radius: 8px;
  z-index:1;
  position:absolute;
}

.simple-link{
  cursor:pointer;
  color:white;
}

.simple-link:hover{
  cursor:pointer;
  color:white;
  text-decoration:none;
}

table, tr, td{
  border-collapse:collapse;
}

@media screen and (max-width: 990px){
  .btn-navbar{
    width:20%;
  }
}

ul{
   list-style-type: none;
}

.form_error{
  left:-10%;
  position:relative;
  text-align:left;
}

/*
.row{
  border: 1px solid black;
}

.col{
  border: 1px solid black;
}*/
</style>

<html>
    <head>
        <meta charset=\"UTF-8\">
        {% block stylesheets %}
        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/minty/bootstrap.min.css\" integrity=\"sha384-H4X+4tKc7b8s4GoMrylmy2ssQYpDHoqzPa9aKXbDwPoPUA3Ra8PA5dGzijN+ePnH\" crossorigin=\"anonymous\">        <link rel=\"stylesheet\" href=\"https://unicons.iconscout.com/release/v4.0.0/css/line.css\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"https://cdn.discordapp.com/attachments/539117854247878686/912349805722877992/logo-removebg-preview.png\">
        <link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
        <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
        <link href=\"https://fonts.googleapis.com/css2?family=Oooh+Baby&display=swap\" rel=\"stylesheet\">
        {% endblock %}

        {% block javascripts %}
        <script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js\" integrity=\"sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js\" integrity=\"sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13\" crossorigin=\"anonymous\"></script>
        {% endblock %}
    </head>
    <body>
    {% if app.user and app.user.isAdmin %}
    <nav class=\"navbar nav-tabs navbar-expand-lg navbar-dark bg-info\">
    {% else %}
    <nav class=\"navbar nav-tabs navbar-expand-lg navbar-dark bg-primary\">
    {% endif %}
    <div class=\"container-fluid\">
    <a class=\"navbar-brand\" href=\"/\"><img src=\"/images/logo-removebg-preview-removebg-preview_1.png\" width=\"45\" /><strong style='font-family: \"Oooh Baby\", cursive;font-size:30px'>Matete</strong></a>
    <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarColor01\" aria-controls=\"navbarColor01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
      <span class=\"navbar-toggler-icon\"></span>
    </button>
  <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">
    <ul class=\"navbar-nav\">
      <li class=\"nav-item active\">
        <a class=\"nav-link btn btn-outline-primary m-1 btn-navbar\" href=\"{{path('carte')}}\">Carte</a>
      </li>
      <li class=\"nav-item active\">
        <a class=\"nav-link btn btn-outline-primary m-1 btn-navbar\" href=\"{{path('lesAnnonces')}}\">Les annonces</a>
      </li>
      <li class=\"nav-item active\">
        <a class=\"nav-link btn btn-outline-primary m-1 btn-navbar\" href=\"/nouveau\">Créer une annonce</a>
      </li>
      {% if app.user and app.user.isAdmin %}
      <li class=\"nav-item dropdown m-1\">
        <a class=\"nav-link dropdown-toggle text-light\" data-bs-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Admin</a>
        <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\" href=\"/users\">Gérer les users</a>
          <div class=\"dropdown-divider\"></div>
          <a class=\"dropdown-item\" href=\"/gestionCategorie\">Gérer les catégories</a>
          <div class=\"dropdown-divider\"></div>
          <a class=\"dropdown-item\" href=\"/statistiques\">Voir les statistiques</a>
        </div>
      </li>
      {% endif %}
    </ul>
    <ul class=\"navbar-nav\" style=\"margin-left:auto; margin-right:30px\">
       <li class=\"nav-item active\">
        <a href=\"/panier\" class=\"nav-link btn btn-outline-primary m-1 btn-navbar\"><i class=\"uil uil-shopping-cart m-1\"></i>Panier</a>
      </li>
    {% if app.user %}
      <li class=\"nav-item active\">
          <a class=\"nav-link btn btn-outline-primary btn-navbar m-1\" href=\"/showProfil\"><i class=\"uil uil-user-square m-1\"></i>Votre profil</a>
      </li>
      <li class=\"nav-item active\">
          <a class=\"nav-link btn btn-outline-danger btn-navbar m-1\" href=\"/logout\"><i class=\"uil uil-user-times m-1\"></i>Se déconnecter</a>
      </li>
    {% else %}
    <li class=\"nav-item active\">
        <a href=\"/login\" class=\"nav-link btn btn-outline-primary m-1 btn-navbar\"><i class=\"uil uil-user-check m-1\"></i>Se connecter</a>
      </li>
      <li class=\"nav-item active\">
          <a class=\"nav-link btn btn-outline-success btn-navbar m-1\" href=\"/inscription\"><i class=\"uil uil-user-plus m-1\"></i>Créer un compte</a>
      </li>
    {% endif %}
    </ul>
  </div>
  </div>
</nav>
{% if app.user and app.user.suspendu %}
<h1><font color=\"red\"> VOTRE COMPTE EST SUSPENDU </h1>
<h3>Contactez le support à <ins>matete-support@gmail.com</ins></font></h3>

{% endif %}
        {% block body %}
        {% endblock %}
    </body>
</html>", "base.html.twig", "/home/jules/matete/website/templates/base.html.twig");
    }
}

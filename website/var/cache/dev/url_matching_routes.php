<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'carte', '_controller' => 'App\\Controller\\MateteController::carte'], null, null, null, false, false, null]],
        '/lesAnnonces' => [[['_route' => 'lesAnnonces', '_controller' => 'App\\Controller\\MateteController::index'], null, null, null, false, false, null]],
        '/nouveau' => [[['_route' => 'annonce_new', '_controller' => 'App\\Controller\\MateteController::add'], null, null, null, false, false, null]],
        '/panier' => [[['_route' => 'panier', '_controller' => 'App\\Controller\\MateteController::showPanier'], null, null, null, false, false, null]],
        '/pdf' => [[['_route' => 'pdf', '_controller' => 'App\\Controller\\MateteController::pdf'], null, null, null, false, false, null]],
        '/inscription' => [[['_route' => 'new_inscription', '_controller' => 'App\\Controller\\SecurityController::registration'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/users' => [[['_route' => 'list_users', '_controller' => 'App\\Controller\\SecurityController::users'], null, null, null, false, false, null]],
        '/gestionCategorie' => [[['_route' => 'gestion_categorie', '_controller' => 'App\\Controller\\SecurityController::gererCategorie'], null, null, null, false, false, null]],
        '/showProfil' => [[['_route' => 'profil_show', '_controller' => 'App\\Controller\\SecurityController::showProfil'], null, null, null, false, false, null]],
        '/deleteProfil' => [[['_route' => 'profil_delete', '_controller' => 'App\\Controller\\SecurityController::deleteProfil'], null, null, null, false, false, null]],
        '/statistiques' => [[['_route' => 'stats', '_controller' => 'App\\Controller\\SecurityController::stats'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|wdt/([^/]++)(*:24)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:69)'
                            .'|router(*:82)'
                            .'|exception(?'
                                .'|(*:101)'
                                .'|\\.css(*:114)'
                            .')'
                        .')'
                        .'|(*:124)'
                    .')'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:159)'
                .')'
                .'|/a(?'
                    .'|nnonce/([^/]++)(*:188)'
                    .'|jouter_panier/([^/]++)/([^/]++)(*:227)'
                .')'
                .'|/edit/([^/]++)(*:250)'
                .'|/delete(?'
                    .'|/([^/]++)(*:277)'
                    .'|Panier/([^/]++)/([^/]++)(*:309)'
                    .'|Categorie/([^/]++)(*:335)'
                    .'|User/([^/]++)(*:356)'
                    .'|Emplacement/([^/]++)(*:384)'
                .')'
                .'|/valider/([^/]++)(*:410)'
                .'|/suspendUser/([^/]++)(*:439)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        159 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        188 => [[['_route' => 'annonce_show', '_controller' => 'App\\Controller\\MateteController::show'], ['id'], null, null, false, true, null]],
        227 => [[['_route' => 'ajouter_panier', '_controller' => 'App\\Controller\\MateteController::addPanier'], ['id', 'from'], null, null, false, true, null]],
        250 => [[['_route' => 'annonce_edit', '_controller' => 'App\\Controller\\MateteController::edit'], ['id'], null, null, false, true, null]],
        277 => [[['_route' => 'annonce_delete', '_controller' => 'App\\Controller\\MateteController::deleteAction'], ['id'], null, null, false, true, null]],
        309 => [[['_route' => 'panier_delete', '_controller' => 'App\\Controller\\MateteController::deletePanier'], ['id', 'from'], null, null, false, true, null]],
        335 => [[['_route' => 'categorie_delete', '_controller' => 'App\\Controller\\SecurityController::delCategorie'], ['id'], null, null, false, true, null]],
        356 => [[['_route' => 'user_delete', '_controller' => 'App\\Controller\\SecurityController::delUser'], ['id'], null, null, false, true, null]],
        384 => [[['_route' => 'emplacement_delete', '_controller' => 'App\\Controller\\SecurityController::delEmplacement'], ['id'], null, null, false, true, null]],
        410 => [[['_route' => 'valider', '_controller' => 'App\\Controller\\SecurityController::valider'], ['token'], null, null, false, true, null]],
        439 => [
            [['_route' => 'user_suspend', '_controller' => 'App\\Controller\\SecurityController::suspendUser'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];

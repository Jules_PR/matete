<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/stats.html.twig */
class __TwigTemplate_275bb2222a8f11829ab2ca7c5702d99374fe4d9dff6d3a5f86db3db17e1d8685 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/stats.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/stats.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/stats.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<script src=\"https://cdn.jsdelivr.net/npm/chart.js\"></script>
<div class='d-flex'>
<div style='margin-left:3%!important;margin-top:3%!important;width:30%!important; height:30%!important'>
  <canvas id=\"categorieDonut\"></canvas>
</div>
<div style='margin-left:10%!important;margin-top:5%!important;width:50%!important; height:50%!important'>
  <canvas id=\"categorieBar\"></canvas>
</div>
</div>
<script>
const labels = JSON.parse('";
        // line 15
        echo json_encode((isset($context["categorieLabels"]) || array_key_exists("categorieLabels", $context) ? $context["categorieLabels"] : (function () { throw new RuntimeError('Variable "categorieLabels" does not exist.', 15, $this->source); })()));
        echo "')

  const data = {
    labels: labels,
    datasets: [{
      label: 'Categories',
      backgroundColor: JSON.parse('";
        // line 21
        echo json_encode((isset($context["categorieColors"]) || array_key_exists("categorieColors", $context) ? $context["categorieColors"] : (function () { throw new RuntimeError('Variable "categorieColors" does not exist.', 21, $this->source); })()));
        echo "'),
    hoverOffset: 4,
    data: JSON.parse('";
        // line 23
        echo json_encode((isset($context["categorieDatas"]) || array_key_exists("categorieDatas", $context) ? $context["categorieDatas"] : (function () { throw new RuntimeError('Variable "categorieDatas" does not exist.', 23, $this->source); })()));
        echo "'),
    }]
  };

  const config = {
    type: 'doughnut',
    data: data,
  };

  const donut = new Chart(
    document.getElementById('categorieDonut'),
    config
  );

//bar
const labels_bar = JSON.parse('";
        // line 38
        echo json_encode((isset($context["categorieLabels"]) || array_key_exists("categorieLabels", $context) ? $context["categorieLabels"] : (function () { throw new RuntimeError('Variable "categorieLabels" does not exist.', 38, $this->source); })()));
        echo "');

const data_bar = {
  labels: labels,
  datasets: [{
    label: 'Annonces',
    data: JSON.parse('";
        // line 44
        echo json_encode((isset($context["categorieDatas"]) || array_key_exists("categorieDatas", $context) ? $context["categorieDatas"] : (function () { throw new RuntimeError('Variable "categorieDatas" does not exist.', 44, $this->source); })()));
        echo "'),
    backgroundColor: JSON.parse('";
        // line 45
        echo json_encode((isset($context["categorieColors"]) || array_key_exists("categorieColors", $context) ? $context["categorieColors"] : (function () { throw new RuntimeError('Variable "categorieColors" does not exist.', 45, $this->source); })()));
        echo "')
  }]
};

const config_bar = {
    type: 'bar',
    data: data_bar,
    options: {
    scales: {
        y: {
            beginAtZero: true
        }
    }
    },
};

const bar = new Chart(
    document.getElementById('categorieBar'),
    config_bar
  );
</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/stats.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 45,  122 => 44,  113 => 38,  95 => 23,  90 => 21,  81 => 15,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

<script src=\"https://cdn.jsdelivr.net/npm/chart.js\"></script>
<div class='d-flex'>
<div style='margin-left:3%!important;margin-top:3%!important;width:30%!important; height:30%!important'>
  <canvas id=\"categorieDonut\"></canvas>
</div>
<div style='margin-left:10%!important;margin-top:5%!important;width:50%!important; height:50%!important'>
  <canvas id=\"categorieBar\"></canvas>
</div>
</div>
<script>
const labels = JSON.parse('{{ categorieLabels|json_encode|raw }}')

  const data = {
    labels: labels,
    datasets: [{
      label: 'Categories',
      backgroundColor: JSON.parse('{{ categorieColors|json_encode|raw }}'),
    hoverOffset: 4,
    data: JSON.parse('{{ categorieDatas|json_encode|raw }}'),
    }]
  };

  const config = {
    type: 'doughnut',
    data: data,
  };

  const donut = new Chart(
    document.getElementById('categorieDonut'),
    config
  );

//bar
const labels_bar = JSON.parse('{{ categorieLabels|json_encode|raw }}');

const data_bar = {
  labels: labels,
  datasets: [{
    label: 'Annonces',
    data: JSON.parse('{{ categorieDatas|json_encode|raw }}'),
    backgroundColor: JSON.parse('{{ categorieColors|json_encode|raw }}')
  }]
};

const config_bar = {
    type: 'bar',
    data: data_bar,
    options: {
    scales: {
        y: {
            beginAtZero: true
        }
    }
    },
};

const bar = new Chart(
    document.getElementById('categorieBar'),
    config_bar
  );
</script>

{% endblock %}", "admin/stats.html.twig", "/home/jules/matete/website/templates/admin/stats.html.twig");
    }
}

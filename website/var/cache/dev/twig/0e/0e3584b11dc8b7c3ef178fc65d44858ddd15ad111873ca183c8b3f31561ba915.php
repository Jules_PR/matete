<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/new.html.twig */
class __TwigTemplate_12db474f2c5ef2a93d23c969d9d3f5207680aaa04f8908647582b2a8e08d2a27 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/new.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/new.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/new.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "user", [], "any", false, false, false, 4), "suspendu", [], "any", false, false, false, 4), false)) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "user", [], "any", false, false, false, 4), "enabled", [], "any", false, false, false, 4), true)))) {
            // line 5
            echo "        ";
            if ((0 === twig_compare((isset($context["emplacementEmpty"]) || array_key_exists("emplacementEmpty", $context) ? $context["emplacementEmpty"] : (function () { throw new RuntimeError('Variable "emplacementEmpty" does not exist.', 5, $this->source); })()), false))) {
                // line 6
                echo "            ";
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 6, $this->source); })()), 'form_start', ["attr" => ["novalidate" => "novalidate"]]);
                echo "
                <div class='center card' style='width:20%;margin-top:2%;align-items:center'>
                    <div class='card-body'>
                    <h1 class='h3 card-title'>Créer une annonce</h1>

                    ";
                // line 11
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "nom", [], "any", false, false, false, 11), 'widget');
                echo "
                    <div class='text-danger form_error'>";
                // line 12
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), "nom", [], "any", false, false, false, 12), 'errors');
                echo "</div>
                    
                    ";
                // line 14
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "description", [], "any", false, false, false, 14), 'widget');
                echo "
                    <div class='text-danger form_error'>";
                // line 15
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "description", [], "any", false, false, false, 15), 'errors');
                echo "</div>

                    ";
                // line 17
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 17, $this->source); })()), "image", [], "any", false, false, false, 17), 'widget');
                echo "
                    <div class='text-danger form_error'>";
                // line 18
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), "image", [], "any", false, false, false, 18), 'errors');
                echo "</div>

                    ";
                // line 20
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 20, $this->source); })()), "quantite", [], "any", false, false, false, 20), 'widget', ["type" => "number"]);
                echo "
                    <div class='text-danger form_error'>";
                // line 21
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "quantite", [], "any", false, false, false, 21), 'errors');
                echo "</div>

                    ";
                // line 23
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "laCategorie", [], "any", false, false, false, 23), 'widget');
                echo "
                    <div class='text-danger form_error'>";
                // line 24
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), "laCategorie", [], "any", false, false, false, 24), 'errors');
                echo "</div>

                    ";
                // line 26
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 26, $this->source); })()), "emplacement", [], "any", false, false, false, 26), 'widget');
                echo "
                    <div class='text-danger form_error'>";
                // line 27
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), "emplacement", [], "any", false, false, false, 27), 'errors');
                echo "</div>
                    <button type=\"submit\" class=\"btn btn-outline-success btn-lg center mt-4 mb-3\">Poster l'annonce</button>
                </div>
                </div>
            ";
                // line 31
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), 'form_end');
                echo "
        ";
            } else {
                // line 33
                echo "            <h1 class='h3 center mt-2 mx-2'>Veuillez d'abord ajouter des emplacements à votre profil en cliquant <a href='/showProfil'>ici<a>.</h1>
        ";
            }
            // line 35
            echo "    ";
        } else {
            // line 36
            echo "    <h3 class='mt-1 mx-2 text-danger'>Vous ne pouvez pas créer d'annonces</h3>
    ";
        }
        // line 38
        echo "
<script>
//évite de pouvoir entrer d'autres caractères non numériques
document.querySelector(\".quantite\").addEventListener(\"keypress\", function (evt) {
    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
    {
        evt.preventDefault();
    }
});
</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 38,  151 => 36,  148 => 35,  144 => 33,  139 => 31,  132 => 27,  128 => 26,  123 => 24,  119 => 23,  114 => 21,  110 => 20,  105 => 18,  101 => 17,  96 => 15,  92 => 14,  87 => 12,  83 => 11,  74 => 6,  71 => 5,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    {% if app.user.suspendu == false and app.user.enabled == true%}
        {% if emplacementEmpty == false %}
            {{ form_start(form, {attr: {novalidate: 'novalidate'}}) }}
                <div class='center card' style='width:20%;margin-top:2%;align-items:center'>
                    <div class='card-body'>
                    <h1 class='h3 card-title'>Créer une annonce</h1>

                    {{ form_widget(form.nom) }}
                    <div class='text-danger form_error'>{{ form_errors(form.nom) }}</div>
                    
                    {{ form_widget(form.description) }}
                    <div class='text-danger form_error'>{{ form_errors(form.description) }}</div>

                    {{ form_widget(form.image) }}
                    <div class='text-danger form_error'>{{ form_errors(form.image) }}</div>

                    {{ form_widget(form.quantite, { 'type':'number' }) }}
                    <div class='text-danger form_error'>{{ form_errors(form.quantite) }}</div>

                    {{ form_widget(form.laCategorie) }}
                    <div class='text-danger form_error'>{{ form_errors(form.laCategorie) }}</div>

                    {{ form_widget(form.emplacement) }}
                    <div class='text-danger form_error'>{{ form_errors(form.emplacement) }}</div>
                    <button type=\"submit\" class=\"btn btn-outline-success btn-lg center mt-4 mb-3\">Poster l'annonce</button>
                </div>
                </div>
            {{ form_end(form) }}
        {% else %}
            <h1 class='h3 center mt-2 mx-2'>Veuillez d'abord ajouter des emplacements à votre profil en cliquant <a href='/showProfil'>ici<a>.</h1>
        {% endif %}
    {% else %}
    <h3 class='mt-1 mx-2 text-danger'>Vous ne pouvez pas créer d'annonces</h3>
    {% endif %}

<script>
//évite de pouvoir entrer d'autres caractères non numériques
document.querySelector(\".quantite\").addEventListener(\"keypress\", function (evt) {
    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
    {
        evt.preventDefault();
    }
});
</script>

{% endblock %}", "matete/new.html.twig", "/home/jules/matete/website/templates/matete/new.html.twig");
    }
}

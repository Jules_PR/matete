<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/index.html.twig */
class __TwigTemplate_9b4d2094426d7f136a59e58227455e04a49e77857684a824f4f815664d392935 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1 class='center mt-2'>Les Annonces</h1>
<select class='form-control mb-3' style=\"width:10%;position:relative;left:45%\" onchange=\"changeCategorie(this)\">
<option value='Tout'>Toutes catégories</option>
";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 7, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
            // line 8
            echo "<option value='";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categorie"], "nom", [], "any", false, false, false, 8), "html", null, true);
            echo "'>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["categorie"], "nom", [], "any", false, false, false, 8), "html", null, true);
            echo "</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</select>
<div class='d-flex justify-content-center'>
    <input id='recherche' value='";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["recherche"]) || array_key_exists("recherche", $context) ? $context["recherche"] : (function () { throw new RuntimeError('Variable "recherche" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "' type='text' class='form-control w-25 mb-3 mx-2' placeholder='Rechercher'>
    <button class='btn btn-primary h-25' onclick=\"recherche();\">Rechercher</button>
</div>
";
        // line 15
        if ((-1 === twig_compare(twig_length_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 15, $this->source); })())), 1))) {
            // line 16
            echo "    <h4 class='center mt-2'>Il n'y a pas d'annonces pour le moment.</h4>
";
        } else {
            // line 18
            echo "    <div class=\"container\">
    <div class=\"row\">
    ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 20, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["annonce"]) {
                // line 21
                echo "        <div class=\"col-md-4 mt-3 Tout ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "laCategorie", [], "any", false, false, false, 21), "nom", [], "any", false, false, false, 21), "html", null, true);
                echo "\" id='";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 21), "html", null, true);
                echo "'>
        <div class=\"card mb-3 h-100\" id='";
                // line 22
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 22), "html", null, true);
                echo "'>
            <div class=\"card-body\">
                <h5 class=\"card-title\"><strong>";
                // line 24
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "nom", [], "any", false, false, false, 24), "html", null, true);
                echo "</strong></h5>
                <h6 class=\"card-subtitle text-muted\">de ";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "leUser", [], "any", false, false, false, 25), "username", [], "any", false, false, false, 25), "html", null, true);
                echo "</h6>
                ";
                // line 26
                if ((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 26, $this->source); })()), "user", [], "any", false, false, false, 26) && twig_in_filter($context["annonce"], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 26, $this->source); })()), "user", [], "any", false, false, false, 26), "lesAnnonces", [], "any", false, false, false, 26)))) {
                    // line 27
                    echo "                    <h5 class='badge bg-info' style='position:absolute; color:white; left:5.5%; top:13%'>Votre annonce</h5>
                ";
                } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,                 // line 28
$context["annonce"], "id", [], "any", false, false, false, 28), twig_get_array_keys_filter((isset($context["panier"]) || array_key_exists("panier", $context) ? $context["panier"] : (function () { throw new RuntimeError('Variable "panier" does not exist.', 28, $this->source); })())))) {
                    // line 29
                    echo "                    <!--badge panier-->
                    <h5 class='badge bg-success' style='position:absolute; color:white; left:5.5%; top:13%'><a class='simple-link' href=\"";
                    // line 30
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 30), "from" => "lesAnnonces"]), "html", null, true);
                    echo "\"><i class='uil uil-times times' style='font-size:13px' onmouseover=\"hover(this);\" onmouseleave=\"noHover(this);\"></i></a> <a href='/panier' class='simple-link'>Dans votre panier</a></h5>
                ";
                }
                // line 32
                echo "            </div>
            <div style='width:95%; height:250px ;margin: 0 auto;'>
                <img class='center w-100 text-center' src=\"";
                // line 34
                echo twig_escape_filter($this->env, ("/uploads/public/uploads/" . twig_get_attribute($this->env, $this->source, $context["annonce"], "image", [], "any", false, false, false, 34)), "html", null, true);
                echo "\" alt=\"Image de l'annonce\" style='border-radius:6px;height:250px;object-fit: cover;'>
            </div>
            <div class=\"card-body\">
                <p class=\"card-text\">";
                // line 37
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "description", [], "any", false, false, false, 37), "html", null, true);
                echo "</p>
                <hr class=\"mt-2\">
                <a href=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_show", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                echo "\" class='btn btn-outline-dark p-2' style='margin-top:3.5%'>Voir plus</a>
                ";
                // line 40
                if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 40, $this->source); })()), "user", [], "any", false, false, false, 40)) {
                    // line 41
                    echo "                    ";
                    if ((!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 41), twig_get_array_keys_filter((isset($context["panier"]) || array_key_exists("panier", $context) ? $context["panier"] : (function () { throw new RuntimeError('Variable "panier" does not exist.', 41, $this->source); })()))) && !twig_in_filter($context["annonce"], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 41, $this->source); })()), "user", [], "any", false, false, false, 41), "lesAnnonces", [], "any", false, false, false, 41)))) {
                        // line 42
                        echo "                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajouter_panier", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 42), "from" => "lesAnnonces"]), "html", null, true);
                        echo "\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    ";
                    }
                    // line 44
                    echo "                    ";
                    if (twig_in_filter($context["annonce"], twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 44, $this->source); })()), "user", [], "any", false, false, false, 44), "lesAnnonces", [], "any", false, false, false, 44))) {
                        // line 45
                        echo "                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 45)]), "html", null, true);
                        echo "\" class='btn btn-outline-info p-2' style='margin-top:3.5%'>Modifier</a>
                    ";
                    }
                    // line 47
                    echo "                ";
                } else {
                    // line 48
                    echo "                    ";
                    if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 48), twig_get_array_keys_filter((isset($context["panier"]) || array_key_exists("panier", $context) ? $context["panier"] : (function () { throw new RuntimeError('Variable "panier" does not exist.', 48, $this->source); })())))) {
                        // line 49
                        echo "                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ajouter_panier", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 49), "from" => "lesAnnonces"]), "html", null, true);
                        echo "\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    ";
                    }
                    // line 51
                    echo "                ";
                }
                // line 52
                echo "            </div>
             <div class='card-footer'>
                <small class=\"text-muted\">";
                // line 54
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "createdAt", [], "any", false, false, false, 54), "d-m-Y"), "html", null, true);
                echo "</small>
            </div>
            </div>
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annonce'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "    </div>
    </div>
";
        }
        // line 62
        echo "
";
        // line 63
        if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 63, $this->source); })())), 0))) {
            // line 64
            echo "<div class='d-flex mt-5 mb-5' style='margin: 0px auto;text-align:center; justify-content: center;'>

";
            // line 66
            if ((1 === twig_compare(((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 66, $this->source); })()) - 1), 0))) {
                // line 67
                echo "    <a class='btn btn-outline-dark rounded-0 mx-1' href=\"/lesAnnonces?page=";
                echo twig_escape_filter($this->env, ((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 67, $this->source); })()) - 1), "html", null, true);
                echo "&recherche=";
                echo twig_escape_filter($this->env, (isset($context["recherche"]) || array_key_exists("recherche", $context) ? $context["recherche"] : (function () { throw new RuntimeError('Variable "recherche" does not exist.', 67, $this->source); })()), "html", null, true);
                echo "\"><i class='uil uil-arrow-left'></i>Précédent</a>
";
            } else {
                // line 69
                echo "    <a class='btn btn-outline-dark rounded-0 mx-1 disabled'><i class='uil uil-arrow-left'></i>Précédent</a>
";
            }
            // line 71
            echo "
";
            // line 72
            if ((1 === twig_compare((isset($context["nb_pages"]) || array_key_exists("nb_pages", $context) ? $context["nb_pages"] : (function () { throw new RuntimeError('Variable "nb_pages" does not exist.', 72, $this->source); })()), 2))) {
                // line 73
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, ((isset($context["nb_pages"]) || array_key_exists("nb_pages", $context) ? $context["nb_pages"] : (function () { throw new RuntimeError('Variable "nb_pages" does not exist.', 73, $this->source); })()) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 74
                    echo "        ";
                    if ((0 === twig_compare(((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 74, $this->source); })()) - 1), $context["i"]))) {
                        // line 75
                        echo "            <a class='btn btn-outline-dark rounded-0 mx-1 active' href=\"/lesAnnonces?page=";
                        echo twig_escape_filter($this->env, ($context["i"] + 1), "html", null, true);
                        echo "&recherche=";
                        echo twig_escape_filter($this->env, (isset($context["recherche"]) || array_key_exists("recherche", $context) ? $context["recherche"] : (function () { throw new RuntimeError('Variable "recherche" does not exist.', 75, $this->source); })()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, ($context["i"] + 1), "html", null, true);
                        echo "</a>
        ";
                    } else {
                        // line 77
                        echo "            <a class='btn btn-outline-dark rounded-0 mx-1' href=\"/lesAnnonces?page=";
                        echo twig_escape_filter($this->env, ($context["i"] + 1), "html", null, true);
                        echo "&recherche=";
                        echo twig_escape_filter($this->env, (isset($context["recherche"]) || array_key_exists("recherche", $context) ? $context["recherche"] : (function () { throw new RuntimeError('Variable "recherche" does not exist.', 77, $this->source); })()), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, ($context["i"] + 1), "html", null, true);
                        echo "</a>
        ";
                    }
                    // line 79
                    echo " ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 81
            echo "
 ";
            // line 82
            if ((-1 === twig_compare(((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 82, $this->source); })()) + 1), (isset($context["nb_pages"]) || array_key_exists("nb_pages", $context) ? $context["nb_pages"] : (function () { throw new RuntimeError('Variable "nb_pages" does not exist.', 82, $this->source); })())))) {
                // line 83
                echo "    <a class='btn btn-outline-dark rounded-0 mx-1' href='/lesAnnonces?page=";
                echo twig_escape_filter($this->env, ((isset($context["page"]) || array_key_exists("page", $context) ? $context["page"] : (function () { throw new RuntimeError('Variable "page" does not exist.', 83, $this->source); })()) + 1), "html", null, true);
                echo "&recherche=";
                echo twig_escape_filter($this->env, (isset($context["recherche"]) || array_key_exists("recherche", $context) ? $context["recherche"] : (function () { throw new RuntimeError('Variable "recherche" does not exist.', 83, $this->source); })()), "html", null, true);
                echo "'><i class='uil uil-arrow-right'></i>Suivant</a>
";
            } else {
                // line 85
                echo "    <a class='btn btn-outline-dark rounded-0 mx-1 disabled'><i class='uil uil-arrow-right'></i>Suivant</a>
";
            }
            // line 87
            echo "</div>
";
        }
        // line 89
        echo "
<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;
    
    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}

function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}

function recherche(){
    var recherche = document.getElementById(\"recherche\").value;
    var url = \"/lesAnnonces?recherche=\" + recherche;
    window.location.href = url;
}
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  304 => 89,  300 => 87,  296 => 85,  288 => 83,  286 => 82,  283 => 81,  276 => 79,  266 => 77,  256 => 75,  253 => 74,  249 => 73,  247 => 72,  244 => 71,  240 => 69,  232 => 67,  230 => 66,  226 => 64,  224 => 63,  221 => 62,  216 => 59,  205 => 54,  201 => 52,  198 => 51,  192 => 49,  189 => 48,  186 => 47,  180 => 45,  177 => 44,  171 => 42,  168 => 41,  166 => 40,  162 => 39,  157 => 37,  151 => 34,  147 => 32,  142 => 30,  139 => 29,  137 => 28,  134 => 27,  132 => 26,  128 => 25,  124 => 24,  119 => 22,  112 => 21,  108 => 20,  104 => 18,  100 => 16,  98 => 15,  92 => 12,  88 => 10,  77 => 8,  73 => 7,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<h1 class='center mt-2'>Les Annonces</h1>
<select class='form-control mb-3' style=\"width:10%;position:relative;left:45%\" onchange=\"changeCategorie(this)\">
<option value='Tout'>Toutes catégories</option>
{% for categorie in categories %}
<option value='{{categorie.nom}}'>{{categorie.nom}}</option>
{% endfor %}
</select>
<div class='d-flex justify-content-center'>
    <input id='recherche' value='{{ recherche }}' type='text' class='form-control w-25 mb-3 mx-2' placeholder='Rechercher'>
    <button class='btn btn-primary h-25' onclick=\"recherche();\">Rechercher</button>
</div>
{% if annonces|length < 1 %}
    <h4 class='center mt-2'>Il n'y a pas d'annonces pour le moment.</h4>
{% else %}
    <div class=\"container\">
    <div class=\"row\">
    {% for annonce in annonces %}
        <div class=\"col-md-4 mt-3 Tout {{annonce.laCategorie.nom}}\" id='{{annonce.id}}'>
        <div class=\"card mb-3 h-100\" id='{{annonce.id}}'>
            <div class=\"card-body\">
                <h5 class=\"card-title\"><strong>{{annonce.nom}}</strong></h5>
                <h6 class=\"card-subtitle text-muted\">de {{annonce.leUser.username}}</h6>
                {% if app.user and annonce in app.user.lesAnnonces %}
                    <h5 class='badge bg-info' style='position:absolute; color:white; left:5.5%; top:13%'>Votre annonce</h5>
                {% elseif annonce.id in panier|keys %}
                    <!--badge panier-->
                    <h5 class='badge bg-success' style='position:absolute; color:white; left:5.5%; top:13%'><a class='simple-link' href=\"{{ path('panier_delete', {'id': annonce.id, 'from': 'lesAnnonces'}) }}\"><i class='uil uil-times times' style='font-size:13px' onmouseover=\"hover(this);\" onmouseleave=\"noHover(this);\"></i></a> <a href='/panier' class='simple-link'>Dans votre panier</a></h5>
                {% endif %}
            </div>
            <div style='width:95%; height:250px ;margin: 0 auto;'>
                <img class='center w-100 text-center' src=\"{{ '/uploads/public/uploads/' ~ annonce.image }}\" alt=\"Image de l'annonce\" style='border-radius:6px;height:250px;object-fit: cover;'>
            </div>
            <div class=\"card-body\">
                <p class=\"card-text\">{{annonce.description}}</p>
                <hr class=\"mt-2\">
                <a href=\"{{ path('annonce_show', {'id': annonce.id}) }}\" class='btn btn-outline-dark p-2' style='margin-top:3.5%'>Voir plus</a>
                {% if app.user %}
                    {% if annonce.id not in panier|keys and annonce not in app.user.lesAnnonces %}
                        <a href=\"{{ path('ajouter_panier', {'id': annonce.id, 'from': 'lesAnnonces'}) }}\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    {% endif %}
                    {% if annonce in app.user.lesAnnonces %}
                        <a href=\"{{ path('annonce_edit', {'id': annonce.id}) }}\" class='btn btn-outline-info p-2' style='margin-top:3.5%'>Modifier</a>
                    {% endif %}
                {% else %}
                    {% if annonce.id not in panier|keys %}
                        <a href=\"{{ path('ajouter_panier', {'id': annonce.id, 'from': 'lesAnnonces'}) }}\" class='btn btn-outline-success' style='margin-top:3.5%'><i class='uil uil-shopping-cart'></i>Ajouter au panier</a>
                    {% endif %}
                {% endif %}
            </div>
             <div class='card-footer'>
                <small class=\"text-muted\">{{annonce.createdAt|date('d-m-Y')}}</small>
            </div>
            </div>
        </div>
    {% endfor %}
    </div>
    </div>
{% endif %}

{% if annonces|length > 0 %}
<div class='d-flex mt-5 mb-5' style='margin: 0px auto;text-align:center; justify-content: center;'>

{% if (page - 1) > 0 %}
    <a class='btn btn-outline-dark rounded-0 mx-1' href=\"/lesAnnonces?page={{ page - 1 }}&recherche={{ recherche }}\"><i class='uil uil-arrow-left'></i>Précédent</a>
{% else %}
    <a class='btn btn-outline-dark rounded-0 mx-1 disabled'><i class='uil uil-arrow-left'></i>Précédent</a>
{% endif %}

{% if nb_pages > 2 %}
{% for i in 0..(nb_pages - 1) %}
        {% if page - 1 == i %}
            <a class='btn btn-outline-dark rounded-0 mx-1 active' href=\"/lesAnnonces?page={{i + 1}}&recherche={{ recherche }}\">{{ i + 1 }}</a>
        {% else %}
            <a class='btn btn-outline-dark rounded-0 mx-1' href=\"/lesAnnonces?page={{i + 1}}&recherche={{ recherche }}\">{{ i + 1 }}</a>
        {% endif %}
 {% endfor %}
{% endif %}

 {% if (page + 1) < nb_pages %}
    <a class='btn btn-outline-dark rounded-0 mx-1' href='/lesAnnonces?page={{ page + 1 }}&recherche={{ recherche }}'><i class='uil uil-arrow-right'></i>Suivant</a>
{% else %}
    <a class='btn btn-outline-dark rounded-0 mx-1 disabled'><i class='uil uil-arrow-right'></i>Suivant</a>
{% endif %}
</div>
{% endif %}

<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;
    
    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}

function hover(element){
    element.className = \"uil uil-times-circle\"
}

function noHover(element){
    element.className = \"uil uil-times\"
}

function recherche(){
    var recherche = document.getElementById(\"recherche\").value;
    var url = \"/lesAnnonces?recherche=\" + recherche;
    window.location.href = url;
}
</script>
{% endblock %}", "matete/index.html.twig", "/home/jules/matete/website/templates/matete/index.html.twig");
    }
}

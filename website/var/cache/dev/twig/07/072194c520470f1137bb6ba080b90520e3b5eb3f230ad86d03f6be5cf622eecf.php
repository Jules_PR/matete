<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/panier.html.twig */
class __TwigTemplate_f3d843f9d382420cec119b808fb41c3d86d4e39522765f4c6e9fd93fe34a7074 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/panier.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/panier.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/panier.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class='center'>
    <h1 class='mt-2'>Votre panier</h1>
    ";
        // line 6
        if ((-1 === twig_compare(twig_length_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 6, $this->source); })())), 1))) {
            // line 7
            echo "        <h4 class='center'>Vous n'avez pas encore d'annonces dans votre panier. Cliquez <a href='";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lesAnnonces");
            echo "' class='text-primary'>ici</a> pour voir les annonces.</h4>
    ";
        } else {
            // line 9
            echo "    <a href=\"/pdf\" class=\"btn btn-outline-info btn-lg mt-2 mb-2\"><i class='uil uil-print'></i> Imprimer le panier</a>
    <div class='container'>
    <div class='row'>
    ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 12, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["annonce"]) {
                // line 13
                echo "        <div class=\"col-md-4 mt-3 Tout ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "laCategorie", [], "any", false, false, false, 13), "nom", [], "any", false, false, false, 13), "html", null, true);
                echo "\" id='";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 13), "html", null, true);
                echo "'>
         <div class=\"card mb-3 h-100\" id='";
                // line 14
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 14), "html", null, true);
                echo "'>
            <div class=\"card-body\">
                <h5 class=\"card-title\"><strong>";
                // line 16
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "nom", [], "any", false, false, false, 16), "html", null, true);
                echo "</strong></h5>
                <h6 class=\"card-subtitle text-muted\">de ";
                // line 17
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "leUser", [], "any", false, false, false, 17), "username", [], "any", false, false, false, 17), "html", null, true);
                echo "</h6>
            </div>
            <div style='width:95%; height:300px ;margin: 0 auto;'>
                <img class='center w-100 text-center' src=\"";
                // line 20
                echo twig_escape_filter($this->env, ("/uploads/public/uploads/" . twig_get_attribute($this->env, $this->source, $context["annonce"], "image", [], "any", false, false, false, 20)), "html", null, true);
                echo "\" alt=\"Image de l'annonce\" style='border-radius:6px;height:250px;object-fit: cover;'>
            </div>
            <div class=\"card-body\">
                <p class=\"card-text\">";
                // line 23
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "description", [], "any", false, false, false, 23), "html", null, true);
                echo "</p>
                <hr class=\"mt-2\">
                <a href=\"";
                // line 25
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_show", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 25)]), "html", null, true);
                echo "\" class='btn btn-outline-dark p-2' style='margin-top:3.5%'>Voir plus</a>
                <a href=\"";
                // line 26
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 26), "from" => "panier"]), "html", null, true);
                echo "\" class='btn btn-outline-warning p-2' style='margin-top:3.5%'>Retirer du panier</a><br>
            </div>
             <div class='card-footer'>
                <small class=\"text-muted\">";
                // line 29
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "createdAt", [], "any", false, false, false, 29), "d-m-Y"), "html", null, true);
                echo "</small>
            </div>
            </div>
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annonce'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "    </div>
    </div>
    ";
        }
        // line 37
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/panier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 37,  143 => 34,  132 => 29,  126 => 26,  122 => 25,  117 => 23,  111 => 20,  105 => 17,  101 => 16,  96 => 14,  89 => 13,  85 => 12,  80 => 9,  74 => 7,  72 => 6,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<div class='center'>
    <h1 class='mt-2'>Votre panier</h1>
    {% if annonces|length < 1 %}
        <h4 class='center'>Vous n'avez pas encore d'annonces dans votre panier. Cliquez <a href='{{ path('lesAnnonces') }}' class='text-primary'>ici</a> pour voir les annonces.</h4>
    {% else %}
    <a href=\"/pdf\" class=\"btn btn-outline-info btn-lg mt-2 mb-2\"><i class='uil uil-print'></i> Imprimer le panier</a>
    <div class='container'>
    <div class='row'>
    {% for annonce in annonces %}
        <div class=\"col-md-4 mt-3 Tout {{annonce.laCategorie.nom}}\" id='{{annonce.id}}'>
         <div class=\"card mb-3 h-100\" id='{{annonce.id}}'>
            <div class=\"card-body\">
                <h5 class=\"card-title\"><strong>{{annonce.nom}}</strong></h5>
                <h6 class=\"card-subtitle text-muted\">de {{annonce.leUser.username}}</h6>
            </div>
            <div style='width:95%; height:300px ;margin: 0 auto;'>
                <img class='center w-100 text-center' src=\"{{ '/uploads/public/uploads/' ~ annonce.image }}\" alt=\"Image de l'annonce\" style='border-radius:6px;height:250px;object-fit: cover;'>
            </div>
            <div class=\"card-body\">
                <p class=\"card-text\">{{annonce.description}}</p>
                <hr class=\"mt-2\">
                <a href=\"{{ path('annonce_show', {'id': annonce.id}) }}\" class='btn btn-outline-dark p-2' style='margin-top:3.5%'>Voir plus</a>
                <a href=\"{{ path('panier_delete', {'id': annonce.id, 'from': 'panier'}) }}\" class='btn btn-outline-warning p-2' style='margin-top:3.5%'>Retirer du panier</a><br>
            </div>
             <div class='card-footer'>
                <small class=\"text-muted\">{{annonce.createdAt|date('d-m-Y')}}</small>
            </div>
            </div>
        </div>
    {% endfor %}
    </div>
    </div>
    {% endif %}
</div>
{% endblock %}", "matete/panier.html.twig", "/home/jules/matete/website/templates/matete/panier.html.twig");
    }
}

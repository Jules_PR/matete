<?php

declare(strict_types=1);

namespace PackageVersions;

use Composer\InstalledVersions;
use OutOfBoundsException;

class_exists(InstalledVersions::class);

/**
 * This class is generated by composer/package-versions-deprecated, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 *
 * @deprecated in favor of the Composer\InstalledVersions class provided by Composer 2. Require composer-runtime-api:^2 to ensure it is present.
 */
final class Versions
{
    /**
     * @deprecated please use {@see self::rootPackageName()} instead.
     *             This constant will be removed in version 2.0.0.
     */
    const ROOT_PACKAGE_NAME = 'symfony/website-skeleton';

    /**
     * Array of all available composer packages.
     * Dont read this array from your calling code, but use the \PackageVersions\Versions::getVersion() method instead.
     *
     * @var array<string, string>
     * @internal
     */
    const VERSIONS          = array (
  'composer/package-versions-deprecated' => '1.11.99.4@b174585d1fe49ceed21928a945138948cb394600',
  'doctrine/annotations' => '1.13.2@5b668aef16090008790395c02c893b1ba13f7e08',
  'doctrine/cache' => '2.2.0@1ca8f21980e770095a31456042471a57bc4c68fb',
  'doctrine/collections' => '1.6.8@1958a744696c6bb3bb0d28db2611dc11610e78af',
  'doctrine/common' => '3.3.0@c824e95d4c83b7102d8bc60595445a6f7d540f96',
  'doctrine/dbal' => '3.3.6@9e7f76dd1cde81c62574fdffa5a9c655c847ad21',
  'doctrine/deprecations' => 'v0.5.3@9504165960a1f83cc1480e2be1dd0a0478561314',
  'doctrine/doctrine-bundle' => '2.6.3@527970d22b8ca6472ebd88d7c42e512550bd874e',
  'doctrine/doctrine-migrations-bundle' => '3.2.2@3393f411ba25ade21969c33f2053220044854d01',
  'doctrine/event-manager' => '1.1.1@41370af6a30faa9dc0368c4a6814d596e81aba7f',
  'doctrine/inflector' => '2.0.4@8b7ff3e4b7de6b2c84da85637b59fd2880ecaa89',
  'doctrine/instantiator' => '1.4.1@10dcfce151b967d20fde1b34ae6640712c3891bc',
  'doctrine/lexer' => '1.2.3@c268e882d4dbdd85e36e4ad69e02dc284f89d229',
  'doctrine/migrations' => '3.4.2@f9b4c8032276460afd9dfa62fb215734b4380d90',
  'doctrine/orm' => '2.12.2@8291a7f09b12d14783ed6537b4586583d155869e',
  'doctrine/persistence' => '2.5.3@d7edf274b6d35ad82328e223439cc2bb2f92bd9e',
  'doctrine/sql-formatter' => '1.1.2@20c39c2de286a9d3262cc8ed282a4ae60e265894',
  'dompdf/dompdf' => 'v1.2.2@5031045d9640b38cfc14aac9667470df09c9e090',
  'egulias/email-validator' => '3.2@a5ed8d58ed0c340a7c2109f587951b1c84cf6286',
  'friendsofphp/proxy-manager-lts' => 'v1.0.12@8419f0158715b30d4b99a5bd37c6a39671994ad7',
  'laminas/laminas-code' => '4.5.1@6fd96d4d913571a2cd056a27b123fa28cb90ac4e',
  'monolog/monolog' => '2.6.0@247918972acd74356b0a91dfaa5adcaec069b6c0',
  'phenx/php-font-lib' => '0.5.4@dd448ad1ce34c63d09baccd05415e361300c35b4',
  'phenx/php-svg-lib' => '0.4.1@4498b5df7b08e8469f0f8279651ea5de9626ed02',
  'phpdocumentor/reflection-common' => '2.2.0@1d01c49d4ed62f25aa84a747ad35d5a16924662b',
  'phpdocumentor/reflection-docblock' => '5.3.0@622548b623e81ca6d78b721c5e029f4ce664f170',
  'phpdocumentor/type-resolver' => '1.6.1@77a32518733312af16a44300404e945338981de3',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.1.2@513e0666f7216c7459170d56df27dfcefe1689ea',
  'psr/event-dispatcher' => '1.0.0@dbefd12671e8a14ec7f180cab83036ed26714bb0',
  'psr/link' => '1.0.0@eea8e8662d5cd3ae4517c9b864493f59fca95562',
  'psr/log' => '1.1.4@d49695b909c3b7628b6289db5479a1c204601f11',
  'sabberworm/php-css-parser' => '8.4.0@e41d2140031d533348b2192a83f02d8dd8a71d30',
  'sensio/framework-extra-bundle' => 'v6.2.6@6bd976c99ef3f78e31c9490a10ba6dd8901076eb',
  'symfony/asset' => 'v5.3.14@fb60377bfa717aef01de9dfe0ce1c371f546de21',
  'symfony/cache' => 'v5.4.9@a50b7249bea81ddd6d3b799ce40c5521c2f72f0b',
  'symfony/cache-contracts' => 'v2.5.1@64be4a7acb83b6f2bf6de9a02cee6dad41277ebc',
  'symfony/config' => 'v5.4.9@8f551fe22672ac7ab2c95fe46d899f960ed4d979',
  'symfony/console' => 'v5.3.16@2e322c76cdccb302af6b275ea2207169c8355328',
  'symfony/dependency-injection' => 'v5.4.9@beecae161577305926ec078c4ed973f2b98880b3',
  'symfony/deprecation-contracts' => 'v2.5.1@e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
  'symfony/doctrine-bridge' => 'v5.3.14@b71eb1d7c7520c75f8905f6cff092d5400105222',
  'symfony/dotenv' => 'v5.3.14@f0408446200edf1a363b01d7ed75a279985c389d',
  'symfony/error-handler' => 'v5.4.9@c116cda1f51c678782768dce89a45f13c949455d',
  'symfony/event-dispatcher' => 'v5.4.9@8e6ce1cc0279e3ff3c8ff0f43813bc88d21ca1bc',
  'symfony/event-dispatcher-contracts' => 'v2.5.1@f98b54df6ad059855739db6fcbc2d36995283fe1',
  'symfony/expression-language' => 'v5.3.14@af6a88b8986d5a20d75fd0cc384e362adbade93d',
  'symfony/filesystem' => 'v5.4.9@36a017fa4cce1eff1b8e8129ff53513abcef05ba',
  'symfony/finder' => 'v5.4.8@9b630f3427f3ebe7cd346c277a1408b00249dad9',
  'symfony/form' => 'v5.3.14@bda43cb6c7a94a65266c0d74bdffb3e500aa2696',
  'symfony/framework-bundle' => 'v5.3.15@fef224d1904da67120fdfe9de3d070f8ba607742',
  'symfony/google-mailer' => 'v5.3.14@737f906b6e5fc204308029035d0945078458e0fb',
  'symfony/http-client' => 'v5.3.14@692eace552581e33f05969180289c93c43911d8e',
  'symfony/http-client-contracts' => 'v2.5.1@1a4f708e4e87f335d1b1be6148060739152f0bd5',
  'symfony/http-foundation' => 'v5.4.9@6b0d0e4aca38d57605dcd11e2416994b38774522',
  'symfony/http-kernel' => 'v5.3.16@a126e33084ed0ed2bf3251942911f26078b8c559',
  'symfony/intl' => 'v5.3.14@a97d73f5aa7578c1a0838d83216f7b2ffdce99e2',
  'symfony/mailer' => 'v5.3.14@8ff0b648d030a7bd79cefe05104cc1d3853523d2',
  'symfony/mime' => 'v5.3.14@2769b338f999a7c53a88e3c124a3d69d7d3feb49',
  'symfony/monolog-bridge' => 'v5.4.3@4b56e17c443e7092895477f047f2a70f324f984c',
  'symfony/monolog-bundle' => 'v3.8.0@a41bbcdc1105603b6d73a7d9a43a3788f8e0fb7d',
  'symfony/notifier' => 'v5.3.14@39635b9c8b8e6b024a4292cdded4c3406ead4f3c',
  'symfony/options-resolver' => 'v5.4.3@cc1147cb11af1b43f503ac18f31aa3bec213aba8',
  'symfony/password-hasher' => 'v5.4.8@bc9c982b25c0292aa4e009b3e9cc9835e4d1e94f',
  'symfony/polyfill-intl-grapheme' => 'v1.25.0@81b86b50cf841a64252b439e738e97f4a34e2783',
  'symfony/polyfill-intl-icu' => 'v1.25.0@c023a439b8551e320cc3c8433b198e408a623af1',
  'symfony/polyfill-intl-idn' => 'v1.25.0@749045c69efb97c70d25d7463abba812e91f3a44',
  'symfony/polyfill-intl-normalizer' => 'v1.25.0@8590a5f561694770bdcd3f9b5c69dde6945028e8',
  'symfony/polyfill-mbstring' => 'v1.25.0@0abb51d2f102e00a4eefcf46ba7fec406d245825',
  'symfony/polyfill-php73' => 'v1.25.0@cc5db0e22b3cb4111010e48785a97f670b350ca5',
  'symfony/polyfill-php80' => 'v1.25.0@4407588e0d3f1f52efb65fbe92babe41f37fe50c',
  'symfony/polyfill-php81' => 'v1.25.0@5de4ba2d41b15f9bd0e19b2ab9674135813ec98f',
  'symfony/process' => 'v5.3.14@8bbae08c19308b9493ad235386144cbefec83cb0',
  'symfony/property-access' => 'v5.3.14@327235376dda28e00c0d96cb030c7d66c62b071b',
  'symfony/property-info' => 'v5.3.14@db50c58877e6e4087bb83abe99404e7f69dd143e',
  'symfony/proxy-manager-bridge' => 'v5.3.14@2b8e3c489432564fe64dfbf3c5c878355b46dbb0',
  'symfony/routing' => 'v5.4.8@e07817bb6244ea33ef5ad31abc4a9288bef3f2f7',
  'symfony/runtime' => 'v5.3.14@affdf1847e37a16dce3667988fc8494ac8bdf92c',
  'symfony/security-bundle' => 'v5.3.14@548b841c02facf4282a1928eb4ebde9aeb9779ba',
  'symfony/security-core' => 'v5.4.8@4540ecb8ae82cc46d9580672888597f481ff0440',
  'symfony/security-csrf' => 'v5.4.9@ac64013bba1c7a6555b3dc4e701f058cf9f7eb64',
  'symfony/security-guard' => 'v5.4.9@64c83d25b5b23fa07e77c861d19e46ce7929a789',
  'symfony/security-http' => 'v5.4.9@6e456f22027e7b114914b7c6f18e18c688441e2e',
  'symfony/serializer' => 'v5.3.14@5d58faa7fa1350aa3d11502cdd80ae2f242404aa',
  'symfony/service-contracts' => 'v2.5.1@24d9dc654b83e91aa59f9d167b131bc3b5bea24c',
  'symfony/stopwatch' => 'v5.3.14@fec0d6b81afabfdf3d2db5c1e4c2107b59699a4a',
  'symfony/string' => 'v5.3.14@006fadf2d23b7b1a0ec5f3a0a5a80e1da2819c94',
  'symfony/translation' => 'v5.3.14@945066809dc18f6e26123098e1b6e1d7a948660b',
  'symfony/translation-contracts' => 'v2.5.1@1211df0afa701e45a04253110e959d4af4ef0f07',
  'symfony/twig-bridge' => 'v5.4.9@fd13c89a1abdbaa7ee2e655d9a11405adcb7a6cf',
  'symfony/twig-bundle' => 'v5.3.14@0bc86c3f5cf7354423a9256dace9fcaeb52bd1ed',
  'symfony/validator' => 'v5.3.14@450191bff1b2a1b01b9b6d3e3a289ac211eb7343',
  'symfony/var-dumper' => 'v5.4.9@af52239a330fafd192c773795520dc2dd62b5657',
  'symfony/var-exporter' => 'v5.4.9@63249ebfca4e75a357679fa7ba2089cfb898aa67',
  'symfony/web-link' => 'v5.3.14@4dee73740953a0233c648b9e5c4db1053dbeb88a',
  'symfony/yaml' => 'v5.3.14@c441e9d2e340642ac8b951b753dea962d55b669d',
  'twig/extra-bundle' => 'v3.4.0@2e58256b0e9fe52f30149347c0547e4633304765',
  'twig/twig' => 'v3.4.1@e939eae92386b69b49cfa4599dd9bead6bf4a342',
  'webmozart/assert' => '1.10.0@6964c76c7804814a842473e0c8fd15bab0f18e25',
  'doctrine/data-fixtures' => '1.5.3@ba37bfb776de763c5bf04a36d074cd5f5a083c42',
  'doctrine/doctrine-fixtures-bundle' => '3.4.2@601988c5b46dbd20a0f886f967210aba378a6fd5',
  'myclabs/deep-copy' => '1.11.0@14daed4296fae74d9e3201d2c4925d1acb7aa614',
  'nikic/php-parser' => 'v4.13.2@210577fe3cf7badcc5814d99455df46564f3c077',
  'phar-io/manifest' => '2.0.3@97803eca37d319dfa7826cc2437fc020857acb53',
  'phar-io/version' => '3.2.1@4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
  'phpspec/prophecy' => 'v1.15.0@bbcd7380b0ebf3961ee21409db7b38bc31d69a13',
  'phpunit/php-code-coverage' => '9.2.15@2e9da11878c4202f97915c1cb4bb1ca318a63f5f',
  'phpunit/php-file-iterator' => '3.0.6@cf1c2e7c203ac650e352f4cc675a7021e7d1b3cf',
  'phpunit/php-invoker' => '3.1.1@5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
  'phpunit/php-text-template' => '2.0.4@5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
  'phpunit/php-timer' => '5.0.3@5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
  'phpunit/phpunit' => '9.5.20@12bc8879fb65aef2138b26fc633cb1e3620cffba',
  'sebastian/cli-parser' => '1.0.1@442e7c7e687e42adc03470c7b668bc4b2402c0b2',
  'sebastian/code-unit' => '1.0.8@1fc9f64c0927627ef78ba436c9b17d967e68e120',
  'sebastian/code-unit-reverse-lookup' => '2.0.3@ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
  'sebastian/comparator' => '4.0.6@55f4261989e546dc112258c7a75935a81a7ce382',
  'sebastian/complexity' => '2.0.2@739b35e53379900cc9ac327b2147867b8b6efd88',
  'sebastian/diff' => '4.0.4@3461e3fccc7cfdfc2720be910d3bd73c69be590d',
  'sebastian/environment' => '5.1.4@1b5dff7bb151a4db11d49d90e5408e4e938270f7',
  'sebastian/exporter' => '4.0.4@65e8b7db476c5dd267e65eea9cab77584d3cfff9',
  'sebastian/global-state' => '5.0.5@0ca8db5a5fc9c8646244e629625ac486fa286bf2',
  'sebastian/lines-of-code' => '1.0.3@c1c2e997aa3146983ed888ad08b15470a2e22ecc',
  'sebastian/object-enumerator' => '4.0.4@5c9eeac41b290a3712d88851518825ad78f45c71',
  'sebastian/object-reflector' => '2.0.4@b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
  'sebastian/recursion-context' => '4.0.4@cd9d8cf3c5804de4341c283ed787f099f5506172',
  'sebastian/resource-operations' => '3.0.3@0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
  'sebastian/type' => '3.0.0@b233b84bc4465aff7b57cf1c4bc75c86d00d6dad',
  'sebastian/version' => '3.0.2@c6c1022351a901512170118436c764e473f6de8c',
  'symfony/browser-kit' => 'v5.3.14@84d0dc472df8dc05b0fa92e39272255fe42fc5d0',
  'symfony/css-selector' => 'v5.3.14@9e3a9e99095fd55fb68c0ffe2f7e10ae13ac66ee',
  'symfony/debug-bundle' => 'v5.3.14@249ceb22eecad97bc1044c9519651777abf2474c',
  'symfony/dom-crawler' => 'v5.4.9@a213cbc80382320b0efdccdcdce232f191fafe3a',
  'symfony/maker-bundle' => 'v1.39.0@f2b99ba44e22a44fcf724affa53b5539b25fde90',
  'symfony/phpunit-bridge' => 'v6.1.0@092ccc3b364925cd8ed6046bc31dcf3a022bd5a4',
  'symfony/web-profiler-bundle' => 'v5.3.14@d0bf5f7ecf8c49594ddc53e5cdb7929f9b401988',
  'theseer/tokenizer' => '1.2.1@34a41e998c2183e22995f158c581e7b5e755ab9e',
  'symfony/polyfill-ctype' => '*@82fe95616cd065b8e9b674d030cf75d780356228',
  'symfony/polyfill-iconv' => '*@82fe95616cd065b8e9b674d030cf75d780356228',
  'symfony/polyfill-php72' => '*@82fe95616cd065b8e9b674d030cf75d780356228',
  'symfony/website-skeleton' => 'dev-main@82fe95616cd065b8e9b674d030cf75d780356228',
);

    private function __construct()
    {
    }

    /**
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function rootPackageName() : string
    {
        if (!self::composer2ApiUsable()) {
            return self::ROOT_PACKAGE_NAME;
        }

        return InstalledVersions::getRootPackage()['name'];
    }

    /**
     * @throws OutOfBoundsException If a version cannot be located.
     *
     * @psalm-param key-of<self::VERSIONS> $packageName
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function getVersion(string $packageName): string
    {
        if (self::composer2ApiUsable()) {
            return InstalledVersions::getPrettyVersion($packageName)
                . '@' . InstalledVersions::getReference($packageName);
        }

        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }

    private static function composer2ApiUsable(): bool
    {
        if (!class_exists(InstalledVersions::class, false)) {
            return false;
        }

        if (method_exists(InstalledVersions::class, 'getAllRawData')) {
            $rawData = InstalledVersions::getAllRawData();
            if (count($rawData) === 1 && count($rawData[0]) === 0) {
                return false;
            }
        } else {
            $rawData = InstalledVersions::getRawData();
            if ($rawData === null || $rawData === []) {
                return false;
            }
        }

        return true;
    }
}
